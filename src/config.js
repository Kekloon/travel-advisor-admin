export const API_URL = process.env.NODE_ENV === 'development'
                      ? `http://localhost:4000/api`
                      : `http://YOUR_DOMAIN/api`
