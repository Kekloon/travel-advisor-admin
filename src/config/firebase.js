import fb from "firebase";
require('firebase/firestore')

var config = {
  apiKey: "AIzaSyBWdcE7IFRznmBietNkn9jYSu6zNUNBAk0",
  authDomain: "travel-advisor-c23cd.firebaseapp.com",
  databaseURL: "https://travel-advisor-c23cd.firebaseio.com",
  projectId: "travel-advisor-c23cd",
  storageBucket: "travel-advisor-c23cd.appspot.com",
  messagingSenderId: "344257327754"
  };

export const firebase = fb.initializeApp(config);
export const storage = firebase.storage()
export const firestore = firebase.firestore()