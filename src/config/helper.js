import { storage } from './firebase'

export const uploadFile = (filePath) => {
    return new Promise((resolve, reject) => {
        let storageRef = storage.ref().child(filePath.uid)
        let uploadTask = storageRef.put(filePath)

        uploadTask.on('state_changed', function(snapshot) {
        }, err => {
            reject(err)
        }, () => {
            uploadTask.then((res) => {
                const downloadURL = res.downloadURL;
                resolve(downloadURL)
            })
        })
    })
}

export const deleteFile = (fileUrl) => {
    return new Promise((resolve, reject) => {
        const fileRef = storage.refFromURL(fileUrl)
        fileRef.delete().then(() => {
            resolve(true)
        }).catch((e) => {
            reject(e)
        })
    })
}