import React, { Component } from 'react'
// import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Switch, Route } from 'react-router-dom'
import { Layout } from 'antd'
import '../App.scss'
// Component Path
import SiderComponent from './SiderComponent'
import ComicListContainer from '../../Comic/container/ComicListContainer'
import AuthorListContainer from '../../Author/container/AuthorListContainer'
import CategoryListContainer from '../../Category/container/CategoryListContainer'
import UserListContainer from '../../User/container/UserListContainer'
import ComicDetails from '../../Comic/container/ComicDetailsContainer'
import ForumContainer from '../../Forum/container/ForumContainer'
import SlideShowContainer from '../../Slideshow/container/SlideshowContainer'
import CountryListContainer from '../../CountryList/container/CountryListContainer'
import DestinationListContainer from '../../Destination/container/DestinationListContainer'
import EditDestinationContainer from '../../Destination/container/EditDestinationContainer'
import HotelListContainer from '../../Hotel/container/HotelListContainer'
import MessageListContainer from '../../Message/container/MessageListContainer'

const { Content } = Layout

class App extends Component {
  // componentDidMount() {
  //   this.props.history.listen(location => {
  //     this.props.dispatch(toggleMenu(false))
  //   })
  // }
  render() {
    return (
      <div>
        <SiderComponent />
        <Layout>
          <Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
            <Switch>
              <Route exact path='/' render={() => <CountryListContainer />} />
              <Route exact path='/countryList' render={() => <CountryListContainer />} />
              <Route exact path='/destinationList/' render={() => <DestinationListContainer />} />
              <Route exact path='/destinationList/detail' render={() => <EditDestinationContainer />} />
              <Route exact path='/hotelList' render={() => <HotelListContainer />} />
              <Route exact path='/messageList' render={() => <MessageListContainer />} />
              <Route exact path='/comic/details' render={() => <ComicDetails />} />
              <Route exact path='/forum' render={() => <ForumContainer />} />
              <Route exact path='/slideshow' render={() => <SlideShowContainer />} />
            </Switch>
          </Content>
        </Layout>
      </div>
    )
  }
}

// App.propTypes = {
//   children: PropTypes.node,
// }

export default connect()(App)
