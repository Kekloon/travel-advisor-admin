import React, { Component } from 'react'
import { Layout, Menu, Icon } from 'antd'
import { NavLink } from 'react-router-dom'

export default class SiderComponent extends Component {
    state = {
        collapsed: false,
    }
    render() {
        return (
            <Layout
                style={{ overflow: 'hidden', display: 'inline' }}>
                <div className='logo' style={{
                    width: '100px',
                    height: '65px',
                    float: 'left',
                    marginLeft: '20px',
                }} >
                    <img style={{ marginTop: '5px', marginLeft: '10px' }} src={require('../../../assets/logo.png')} width='60px' height='50px'
                    />
                </div>
                <Menu mode='horizontal' defaultSelectedKeys={['1']} theme='dark' style={{ lineHeight: '65px' }} >
                    <Menu.Item key='1'>
                        <NavLink to='/countryList'>
                            <Icon className='nav-text' type='global' />
                            <span className='nav-text'>Country List</span>
                        </NavLink>
                    </Menu.Item>
                    <Menu.Item key='2'>
                        <NavLink to='/destinationList'>
                            <Icon className='nav-text' type='aliwangwang' />
                            <span className='nav-text'>Destination List</span>
                        </NavLink>
                    </Menu.Item>
                    <Menu.Item key='3'>
                        <NavLink to='/hotelList'>
                            <Icon className='nav-text' type='bar-chart' />
                            <span className='nav-text'>Hotel List</span>
                        </NavLink>
                    </Menu.Item>
                    <Menu.Item key='4'>
                        <NavLink to='/messageList'>
                            <Icon style={{ fontSize: 20 }} type='wechat' />
                            <span className='nav-text'>Message List</span>
                        </NavLink>
                    </Menu.Item>
                </Menu>
            </Layout>
        )
    }
}
