import { firestore } from '../../config/firebase'
import { message } from 'antd'

const checkIsNameExist = (itemName) => {
    return new Promise((resolve, reject) => {
        let query = firestore.collection('authors').where('name', '==', itemName)
        query.get().then((val => {
            if (val.docs.length > 0) {
                resolve(true)
                message.error('Author has already exist!')
            } else {
                resolve(false)
            }
        })).catch((e) => reject(e))
    })
}

// -------------------------------------- Get Authors list ------------------------------------- //
export const getAuthorList = () => async (dispatch) => {
    try {
        dispatch(_requestGetAuthors())
        firestore.collection('authors').onSnapshot((val) => {
            firestore.collection("authors").get().then((querySnapshot) => {
                let data = []
                querySnapshot.forEach((doc) => {
                    data.push(doc.data())
                });
                dispatch(_successGetAuthors(data))
            }).catch((e) => dispatch(_failedGetAuthors(e)))
        })

    } catch (e) {
        dispatch(_failedGetAuthors(e))
    }
}

export const _requestGetAuthors = () => ({
    type: 'REQUEST_GET_AUTHORS'
})
export const _successGetAuthors = (payload) => ({
    type: 'SUCCESS_GET_AUTHORS',
    payload
})
export const _failedGetAuthors = (err) => ({
    type: 'FAILED_GET_AUTHORS',
    err
})


// -------------------------------------- Add new author ------------------------------------- //
export const addNewAuthor = (item) => async (dispatch) => {
    const result = await checkIsNameExist(item.name)
    if (!result) {
        console.log("Action add author")
        dispatch(_requestAddNewAuthor())
        let AuthorsRef = firestore.collection("authors")

        AuthorsRef.add({}).then((doc) => {
            AuthorsRef.doc(doc.id).set({
                id: doc.id,
                name: item.name
            })
        }).then(() => dispatch(_successAddNewAuthor())).then(() => message.success('Add new author successfully'))
            .catch((e) => dispatch(_failedAddNewAuthor(e)))
    }
}


export const _requestAddNewAuthor = () => ({
    type: 'REQUEST_ADD_NEW_AUTHOR'
})
export const _successAddNewAuthor = () => ({
    type: 'SUCCESS_ADD_NEW_AUTHOR'
})
export const _failedAddNewAuthor = (err) => ({
    type: 'FAILED_ADD_NEW_AUTHOR',
    err
})

// -------------------------------------- Delete author ------------------------------------- //
export const deleteAuthor = (item) => async (dispatch) => {
    dispatch(_requestDeleteAuthor())
    let AuthosrRef = firestore.collection("authors").doc(item.id)
    AuthosrRef.delete()
        .then(() => dispatch(_successDeleteAuthor())).then(() => message.success('Delete author successfully'))
        .catch((e) => dispatch(_failedDeleteAuthor(e)))
}

export const _requestDeleteAuthor = () => ({
    type: 'REQUEST_DELETE_AUTHOR'
})
export const _successDeleteAuthor = () => ({
    type: 'SUCCESS_DELETE_AUTHOR'
})
export const _failedDeleteAuthor = (err) => ({
    type: 'FAILED_DELETE_AUTHOR',
    err
})

// -------------------------------------- Update author ------------------------------------- //
export const updateAuthor = (item) => async (dispatch) => {
    dispatch(_requestUpdateAuthor())
    const ref = firestore.collection('authors').doc(item.id)

    ref.set({
        name: item.name
    }, { merge: true })
        .then(() => dispatch(_successUpdateAuthor()))
        .then(() => message.success('Update successfully'))
        .catch((e) => dispatch(_failedUpdateAuthor(e)))

}

export const _requestUpdateAuthor = () => ({
    type: 'REQUEST_UPDATE_AUTHOR'
})
export const _successUpdateAuthor = () => ({
    type: 'SUCCESS_UPDATE_AUTHOR'
})
export const _failedUpdateAuthor = (e) => ({
    type: 'FAILED_UPDATE_AUTHOR',
    err
})