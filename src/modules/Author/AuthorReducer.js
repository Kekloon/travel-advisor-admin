const initialState = {
    isLoading: false,
    errorMessage: "",
    success: false,
    authorList: []
}

export default function (state = initialState, action) {
    switch (action.type) {

        // Get AUTHOR --------------------------------------------   
        case 'REQUEST_GET_AUTHORS':
            return Object.assign({}, state, {
                isLoading: true
            })
        case 'SUCCESS_GET_AUTHORS':
            return Object.assign({}, state, {
                isLoading: false,
                authorList: action.payload
            })
        case 'FAILED_GET_AUTHORS':
            return Object.assign({}, state, {
                isLoading: false,
                errorMessage: action.err
            })

        // Add AUTHOR --------------------------------------------       
        case 'REQUEST_ADD_NEW_AUTHOR':
            return Object.assign({}, state, {
                isLoading: true,
                success: false
            })
        case 'SUCCESS_ADD_NEW_AUTHOR':
            return Object.assign({}, state, {
                isLoading: false,
                success: true
            })
        case 'FAILED_ADD_NEW_AUTHOR':
            return Object.assign({}, state, {
                isLoading: false,
                success: false,
                errorMessage: action.err
            })

        // Delete AUTHOR --------------------------------------------   
        case 'REQUEST_DELETE_AUTHOR':
            return Object.assign({}, state, {
                isLoading: true
            })
        case 'SUCCESS_DELETE_AUTHOR':
            return Object.assign({}, state, {
                isLoading: false
            })
        case 'FAILED_DELETE_AUTHOR':
            return Object.assign({}, state, {
                isLoading: false,
                errorMessage: action.err
            })

        // Update AUTHOR --------------------------------------------          
        case 'REQUEST_UPDATE_AUTHOR':
            return Object.assign({}, state, {
                isLoading: true,
                success: false
            })
        case 'SUCCESS_UPDATE_AUTHOR':
            return Object.assign({}, state, {
                isLoading: false,
                success: true
            })
        case 'FAILED_UPDATE_AUTHOR':
            return Object.assign({}, state, {
                isLoading: false,
                success: false
            })
        default:
            return state
    }
}