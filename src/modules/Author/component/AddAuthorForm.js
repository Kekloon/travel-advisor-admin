import React, { Component } from 'react'
import { Form, Input, Button, Upload, Icon, Spin } from 'antd';
const FormItem = Form.Item;

class AddAuthorForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            author: {},
            isLoading: false
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isLoading: nextProps.isLoading
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                this.setState({
                    author: {
                        name: values.authorName,
                    },
                    isLoading: true
                }, () => this.props.addNewAuthor(this.state.author))
            }
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 8,
                },
            },
        };

        return (
            <Spin spinning={this.state.isLoading} >
                <Form onSubmit={this.handleSubmit}>
                    <FormItem
                        {...formItemLayout}
                        label="Author name"
                    >
                        {getFieldDecorator('authorName', {
                            rules: [{ required: true, message: 'Please input author name!', whitespace: true }],
                        })(
                            <Input />
                        )}
                    </FormItem>
                    <FormItem {...tailFormItemLayout}>
                        <Button type="primary" htmlType="submit" >Add</Button>
                    </FormItem>
                </Form>
            </Spin>
        );
    }
}

const WrappedAddAuthorForm = Form.create()(AddAuthorForm);
export default WrappedAddAuthorForm