import React, { Component } from 'react'
import { List, Avatar, Button, Spin, Modal, message, Popconfirm } from 'antd';
const confirm = Modal.confirm
import AddAuthorFormContainer from '../container/AddAuthorFormContainer'
import EditAuthorFormContainer from '../container/EditAuthorFormContainer'

class AuthorList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            authorList: [],
            authorItem: null,
            modalType: 'Add'
        }
    }

    componentWillMount() {
        this.props.getAuthorList()
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isLoading: nextProps.isLoading,
            authorList: nextProps.authorList
        })
        nextProps.success ? this.setModalVisible(false) : null
    }

    setModalVisible(bool, type, item) {
        this.setState({
            modalVisible: bool,
            modalType: type,
            authorItem: item
        })
    }

    showModal() {
        const { modalType, authorItem, isLoading } = this.state

        if (modalType == "Add") {
            return (
                <Modal
                    title="New Author"
                    wrapClassName="vertical-center-modal"
                    visible={this.state.modalVisible}
                    onCancel={() => this.setModalVisible(false)}
                    footer={[]}
                >
                    <div>
                        <AddAuthorFormContainer />
                    </div>
                </Modal>
            )
        } else if (modalType == "Edit") {
            return (
                <Modal
                    title="Edit Author"
                    wrapClassName="vertical-center-modal"
                    visible={this.state.modalVisible}
                    onCancel={() => this.setModalVisible(false)}
                    footer={[]}
                >
                    <div>
                        <EditAuthorFormContainer item={authorItem}/>
                    </div>
                </Modal>
            )
        }
    }

    showDeleteConfirm(item) {
        confirm({
            title: 'Are you sure delete this author?',
            content: item.name,
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk: () => {
                this.props.deleteAuthor(item)
            },
            onCancel() { },
        })
    }

    render() {
        const { isLoading, authorList, modalVisible } = this.state
        const loadMore = this.showModal()

        return (

            <div>
                <div style={{ width: '100%', display: 'table' }}>
                    <h3 style={{ display: 'table-cell' }} >Author List</h3>
                    <div style={{ textAlign: 'right', display: 'table-cell' }}>
                        <Button onClick={() => this.setModalVisible(true, 'Add')} type="primary">Add new author</Button>
                    </div>
                </div>
                <List
                    className="demo-loadmore-list"
                    loading={isLoading}
                    itemLayout="horizontal"
                    loadMore={loadMore}
                    dataSource={authorList}
                    renderItem={item => (
                        <List.Item
                            actions={[
                                <Button onClick={() => this.setModalVisible(true, 'Edit', item)}>Edit</Button>,
                                <Button onClick={() => this.showDeleteConfirm(item)}>Delete</Button>]}
                        >
                            <List.Item.Meta
                                title={item.name}
                                description=""
                            />
                        </List.Item>
                    )}
                />
            </div>
        )
    }
}

export default AuthorList