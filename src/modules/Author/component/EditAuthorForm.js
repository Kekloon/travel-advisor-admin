import React, { Component } from 'react'
import { Form, Input, Button, Upload, Icon, Spin } from 'antd';
const FormItem = Form.Item;

class EditAuthorForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            author: {},
            isLoading: false
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isLoading: nextProps.isLoading
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                this.setState({
                    author:{
                        id: this.props.item.id,
                        name: values.authorName
                    },
                    isLoading: true
                }, () => this.props.updateAuthor(this.state.author))
            }
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 8,
                },
            },
        };

        return (
            <Spin spinning={this.state.isLoading} >
                <Form onSubmit={this.handleSubmit}>
                    <FormItem
                        {...formItemLayout}
                        label="Author name"
                    >
                        {getFieldDecorator('authorName', {
                            rules: [{ required: true, message: 'Please input author name!', whitespace: true }],
                            initialValue: this.props.item.name
                        })(
                            <Input />
                        )}
                    </FormItem>
                    <FormItem {...tailFormItemLayout}>
                        <Button type="primary" htmlType="submit" >Update</Button>
                    </FormItem>
                </Form>
            </Spin>
        );
    }
}

const WrappedEditAuthorForm = Form.create()(EditAuthorForm);
export default WrappedEditAuthorForm