import { connect } from 'react-redux'
import { addNewAuthor } from '../AuthorAction'
import AddAuthorForm from '../component/AddAuthorForm'

const mapStateToProps = (state) => ({
    isLoading: state.category.isLoading
})

const mapDispatchToProps = (dispatch) => ({
    addNewAuthor: (item) => {
        dispatch(addNewAuthor(item))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(AddAuthorForm)