import { connect } from 'react-redux'
import AuthorList from '../component/AuthorList'
import { getAuthorList, deleteAuthor } from '../AuthorAction' 

const mapStateToProps = (state) => ({
    isLoading: state.author.isLoading,
    authorList: state.author.authorList,
    success: state.author.success
})

const mapDispatchToProps = (dispatch) => ({
    getAuthorList: () => {
        dispatch(getAuthorList())
    },
    deleteAuthor: (item) => {
        dispatch(deleteAuthor(item))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(AuthorList)