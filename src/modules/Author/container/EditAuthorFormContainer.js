import { connect } from 'react-redux'
import EditAuthorForm from '../component/EditAuthorForm'
import { updateAuthor } from '../AuthorAction'

const mapStateToProps = (state) => ({
    isLoading: state.category.isLoading
})

const mapDispatchToProps = (dispatch) => ({
    updateAuthor: (item) => {
        dispatch(updateAuthor(item))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(EditAuthorForm)