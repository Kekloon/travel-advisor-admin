import { storage, firestore } from '../../config/firebase'
import { uploadFile } from '../../config/helper'
import { message } from 'antd'

const checkIsNameExist = (itemName) => {
    return new Promise((resolve, reject) => {
        let query = firestore.collection('categories').where('name', '==', itemName)
        query.get().then((val => {
            if (val.docs.length > 0) {
                resolve(true)
                message.error('Category name has already exist!')
            } else {
                resolve(false)
            }
        })).catch((e) => reject(e))
    })
}

// -------------------------------------- Get Categories list ------------------------------------- //
export const getCategories = () => async (dispatch) => {
    try {
        dispatch(_requestGetCategories())
        firestore.collection('categories').onSnapshot((val) => {
            firestore.collection("categories").get().then((querySnapshot) => {
                let data = []
                querySnapshot.forEach((doc) => {
                    data.push(doc.data())
                })
                dispatch(_successGetCategories(data))
            }).catch((e) => dispatch(_failedGetCategories(e)))
        })
    } catch (e) {
        dispatch(_failedGetCategories(e))
    }
}

export const _requestGetCategories = () => ({
    type: 'REQUEST_GET_CATEGORIES'
})
export const _successGetCategories = (payload) => ({
    type: 'SUCCESS_GET_CATEGORIES',
    payload
})
export const _failedGetCategories = (err) => ({
    type: 'FAILED_GET_CATEGORIES',
    err
})

// -------------------------------------- Add new category ------------------------------------- //
export const addNewCategory = (category) => async (dispatch) => {
    const result = await checkIsNameExist(category.name)

    if (!result) {
        dispatch(_requestAddNewCategory())
        let categoriesRef = firestore.collection("categories")
        let fileUrl = await uploadFile(category.file)

        categoriesRef.add({}).then((doc) => {
            categoriesRef.doc(doc.id).set({
                id: doc.id,
                name: category.name,
                coverImage: fileUrl
            })
        }).then(() => dispatch(_successAddNewCategory())).then(() => message.success('Add new category successfully'))
            .catch((e) => dispatch(_failedAddNewCategory(e)))
    }
}

export const _requestAddNewCategory = () => ({
    type: 'REQUEST_ADD_NEW_CATEGORY'
})
export const _successAddNewCategory = () => ({
    type: 'SUCCESS_ADD_NEW_CATEGORY'
})
export const _failedAddNewCategory = (err) => ({
    type: 'FAILED_ADD_NEW_CATEGORY',
    err
})

// -------------------------------------- Delete category ------------------------------------- //
export const deleteCategory = (item) => async (dispatch) => {
    dispatch(_requestDeleteCategory())

    let categoriesRef = firestore.collection("categories").doc(item.id)
    let file = storage.refFromURL(item.coverImage)
    file.delete()
        .then(() => categoriesRef.delete()
            .then(() => dispatch(_successDeleteCategory())).then(() => message.success('Delete category successfully'))
            .catch((e) => dispatch(_failedDeleteCategory(e)))
        ).catch((e) => dispatch(_failedDeleteCategory(e)))
}

export const _requestDeleteCategory = () => ({
    type: 'REQUEST_DELETE_CATEGORY'
})
export const _successDeleteCategory = () => ({
    type: 'SUCCESS_DELETE_CATEGORY'
})
export const _failedDeleteCategory = (err) => ({
    type: 'FAILED_DELETE_CATEGORY',
    err
})

// -------------------------------------- Update category ------------------------------------- //
export const updateCategory = (item) => async (dispatch) => {

    dispatch(_requestUpdateCategory())
    const ref = firestore.collection('categories').doc(item.id)

    if (item.file) {
        console.log(item.file)
        let file = storage.refFromURL(item.coverImage)
        let fileUrl = await uploadFile(item.file)
        file.delete()
            .then(() => ref.set({
                name: item.name,
                coverImage: fileUrl
            }, { merge: true })
                .then(() => dispatch(_successUpdateCategory()))
                .then(() => message.success('Update successfully'))
                .catch((e) => dispatch(_failedUpdateCategory(e)))
            )
            .catch((e) => dispatch(_failedUpdateCategory(e)))
    } else {
        ref.set({
            name: item.name
        }, { merge: true })
            .then(() => dispatch(_successUpdateCategory()))
            .then(() => message.success('Update successfully'))
            .catch((e) => dispatch(_failedUpdateCategory(e)))
    }

}

export const _requestUpdateCategory = () => ({
    type: 'REQUEST_UPDATE_CATEGORY'
})
export const _successUpdateCategory = () => ({
    type: 'SUCCESS_UPDATE_CATEGORY'
})
export const _failedUpdateCategory = (err) => ({
    type: 'FAILED_UPDATE_CATEGORY',
    err
})