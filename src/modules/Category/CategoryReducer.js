const initialState = {
    isLoading: false,
    errorMessage: "",
    success: false,
    categoryList: []
}

export default function (state = initialState, action) {
    switch (action.type) {

        // Get Category --------------------------------------------   
        case 'REQUEST_GET_CATEGORIES':
            return Object.assign({}, state, {
                isLoading: true
            })
        case 'SUCCESS_GET_CATEGORIES':
            return Object.assign({}, state, {
                isLoading: false,
                categoryList: action.payload
            })
        case 'FAILED_GET_CATEGORIES':
            return Object.assign({}, state, {
                isLoading: false,
                errorMessage: action.err
            })

        // Add Category --------------------------------------------       
        case 'REQUEST_ADD_NEW_CATEGORY':
            return Object.assign({}, state, {
                isLoading: true,
                success: false
            })
        case 'SUCCESS_ADD_NEW_CATEGORY':
            return Object.assign({}, state, {
                isLoading: false,
                success: true
            })
        case 'FAILED_ADD_NEW_CATEGORY':
            return Object.assign({}, state, {
                isLoading: false,
                success: false,
                errorMessage: action.err
            })

        // Delete Category --------------------------------------------   
        case 'REQUEST_DELETE_CATEGORY':
            return Object.assign({}, state, {
                isLoading: true
            })
        case 'SUCCESS_DELETE_CATEGORY':
            return Object.assign({}, state, {
                isLoading: false
            })
        case 'FAILED_DELETE_CATEGORY':
            return Object.assign({}, state, {
                isLoading: false,
                errorMessage: action.err
            })

        // Update Category --------------------------------------------          
        case 'REQUEST_UPDATE_CATEGORY':
            return Object.assign({}, state, {
                isLoading: true,
                success: false
            })
        case 'SUCCESS_UPDATE_CATEGORY':
            return Object.assign({}, state, {
                isLoading: false,
                success: true
            })
        case 'FAILED_UPDATE_CATEGORY':
            return Object.assign({}, state, {
                isLoading: false,
                success: false
            })
        default:
            return state
    }
}