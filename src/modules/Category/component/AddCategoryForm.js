import React, { Component } from 'react'
import { Form, Input, Button, Upload, Icon, Spin } from 'antd';
const FormItem = Form.Item;

class AddCategoryForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            category: {},
            isLoading: false
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isLoading: nextProps.isLoading
        })
    }

    handleSubmit = (e) => {
        e.preventDefault()
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                this.setState({
                    category: {
                        name: values.categoryName,
                        file: values.upload[0].originFileObj
                    }
                }, () => this.props.addNewCategory(this.state.category))
            }
        })
    }

    normFile = (e) => {
        if (e.fileList.length <= 1) {
            if (Array.isArray(e)) {
                return e;
            }
            return e && e.fileList;
        }
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: { xs: { span: 24 }, sm: { span: 8 }, },
            wrapperCol: { xs: { span: 24 }, sm: { span: 16 }, },
        }
        const tailFormItemLayout = {
            wrapperCol: { xs: { span: 24, offset: 0, }, sm: { span: 16, offset: 8, }, }
        }

        return (
            <Spin spinning={this.state.isLoading} >
                <Form onSubmit={this.handleSubmit}>
                    <FormItem {...formItemLayout} label="Category name">
                        {getFieldDecorator('categoryName', { rules: [{ required: true, message: 'Please input category name!', whitespace: true }], })(<Input />)}
                    </FormItem>
                    <FormItem {...formItemLayout} label="Cover image">
                        {getFieldDecorator('upload', { valuePropName: 'fileList', getValueFromEvent: this.normFile, rules: [{ required: true }] })
                            (
                            <Upload name="logo" action="" listType="picture">
                                <Button>
                                    <Icon type="upload" /> Click to upload
                                </Button>
                            </Upload>
                            )}
                    </FormItem>
                    <FormItem {...tailFormItemLayout}>
                        <Button type="primary" htmlType="submit" >Add</Button>
                    </FormItem>
                </Form>
            </Spin>
        );
    }
}

const WrappedAddCategoryForm = Form.create()(AddCategoryForm);
export default WrappedAddCategoryForm