import React, { Component } from 'react'
import AddFormContainer from '../container/AddFormContainer'
import EditFormContainer from '../container/EditFormContainer'
import { List, Button, Modal } from 'antd';
const confirm = Modal.confirm


class CategoryList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
            categoryList: [],
            modalVisible: false,
            modalType: 'Add',
            categoryItem: null
        }
        this.setModalVisible = this.setModalVisible.bind(this)
        this.showDeleteConfirm = this.showDeleteConfirm.bind(this)
    }

    componentWillMount() {
        this.props.getCategories()
    }
    
    componentWillReceiveProps(nextProps) {
        this.setState({
            isLoading: nextProps.isLoading,
            categoryList: nextProps.categoryList
        })
        nextProps.success ? this.setModalVisible(false) : null
    }
    
    setModalVisible(bool, type, item) {
        this.setState({
            modalVisible: bool,
            modalType: type,
            categoryItem: item
        })
    }
    
    showModal() {
        const { modalType, categoryItem, isLoading } = this.state
        
        if (modalType == "Add") {
            return (
                <Modal
                title="New Category"
                wrapClassName="vertical-center-modal"
                visible={this.state.modalVisible}
                onCancel={() => this.setModalVisible(false)}
                footer={[]}
                >
                    <div>
                        <AddFormContainer />
                    </div>
                </Modal>
            )
        } else if (modalType == "Edit") {
            return (
                <Modal
                title="Edit Category"
                wrapClassName="vertical-center-modal"
                visible={this.state.modalVisible}
                onCancel={() => this.setModalVisible(false)}
                footer={[]}
                >
                    <div>
                        <EditFormContainer item={categoryItem} />
                    </div>
                </Modal>
            )
        }
    }
    
    showDeleteConfirm(item) {
        confirm({
            title: 'Are you sure delete this category?',
            content: item.name,
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk:() => {
                this.props.deleteCategory(item)
            },
            onCancel() {},
        })
    }
    
    render() {
        const { isLoading, categoryList, modalVisible } = this.state
        const loadMore = this.showModal()
        
        return (
            
            <div>
                <div style={{ width: '100%', display: 'table' }}>
                    <h3 style={{ display: 'table-cell' }} >Category List</h3>
                    <div style={{ textAlign: 'right', display: 'table-cell' }}>
                        <Button onClick={() => this.setModalVisible(true, 'Add')} type="primary">Add new Category</Button>
                    </div>
                </div>
                <List
                    className="demo-loadmore-list"
                    loading={isLoading}
                    itemLayout="horizontal"
                    loadMore={loadMore}
                    dataSource={categoryList}
                    renderItem={item => (
                        <List.Item
                            actions={[
                                <Button onClick={() => this.setModalVisible(true, 'Edit', item)}>Edit</Button>,
                                <Button onClick={() => this.showDeleteConfirm(item)}>Delete</Button>]}
                        >
                            <List.Item.Meta
                                avatar={<img src={item.coverImage} height="90px" />}
                                title={item.name}
                                description="Image will show as ratio 4:3 with cover mode in mobile app."
                            />
                        </List.Item>
                    )}
                />
            </div>
        )
    }
}

export default CategoryList