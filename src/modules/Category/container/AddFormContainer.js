import { connect } from 'react-redux'
import AddCategoryForm from '../component/AddCategoryForm'
import { addNewCategory } from '../CategoryAction'

const mapStateToProps = (state) => ({
    isLoading: state.category.isLoading
})

const mapDispatchToProps = (dispatch) => ({
    addNewCategory: (category) => {
        dispatch(addNewCategory(category))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(AddCategoryForm)