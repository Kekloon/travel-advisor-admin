import { connect } from 'react-redux'
import CategoryListComponent from '../component/CategoryList'
import { getCategories, deleteCategory } from '../CategoryAction'


const mapStateToProps = (state) => ({
    isLoading: state.category.isLoading,
    categoryList: state.category.categoryList,
    success: state.category.success
})

const mapDispatchToProps = (dispatch) => ({
    getCategories: () => {
        dispatch(getCategories())
    },
    deleteCategory: (fileUrl) => {
        dispatch(deleteCategory(fileUrl))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(CategoryListComponent)