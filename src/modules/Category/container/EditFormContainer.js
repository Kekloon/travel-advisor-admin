import { connect } from 'react-redux'
import EditCategoryForm from '../component/EditCategoryForm'
import { updateCategory } from '../CategoryAction'

const mapStateToProps = (state) => ({
    isLoading: state.category.isLoading
})

const mapDispatchToProps = (dispatch) => ({
    updateCategory: (category) => {
        dispatch(updateCategory(category))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(EditCategoryForm)