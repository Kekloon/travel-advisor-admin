import { firestore, storage } from '../../config/firebase'
import { uploadFile } from '../../config/helper'
import { message } from 'antd'

const checkIsTitleExist = (title) => {
    return new Promise((resolve, reject) => {
        let query = firestore.collection('comics').where('title', '==', title)
        query.get().then((val) => {
            if (val.docs.length > 0) {
                resolve(true)
                message.error('Comic title has already exist!')
            } else {
                resolve(false)
            }
        }).catch((e) => reject(e))
    })
}

export const setComic = (payload) => ({
    type: 'SET_COMIC',
    payload
})

// -------------------------------------- Get comics ------------------------------------- //
export const getComics = () => (dispatch) => {
    dispatch(_requestGetComic())
    try {
        firestore.collection('comics').onSnapshot(() => {
            // let categoriesObject = 'categoriesObject.'
            // let id = 'vay2tPYi9GwOtLs7Mn9P'
            // .where(categoriesObject + id, '==', true)
            firestore.collection('comics').get().then(async (val) => {
                let comics = []
                const comicList = []
                val.forEach((doc) => {
                    comicList.push(doc.data())
                })

                await Promise.all(comicList.map(async (comic) => {
                    if (comic.author && comic.categories) {
                        const author = await _getAuthorData(comic.author)
                        const categories = await _getCategoryListData(comic.categories)
                        comics.push({
                            ...comic,
                            author,
                            categories
                        })
                    }
                }))
                dispatch(_successGetComic(comics))
            }).catch((e) => dispatch(_failedGetComic(e)))
        })
    } catch (e) {
        dispatch(_failedGetComic(e))
    }
}

const _getAuthorData = (author) => {
    return new Promise((resolve, reject) => {
        author.onSnapshot((author) => {
            resolve(author.data())
        }, (e) => {
            reject(e)
        })
    })
}

const _getCategoryData = (category) => {
    return new Promise((resolve, reject) => {
        category.onSnapshot((category) => {
            resolve(category.data())
        }, (e) => {
            reject(e)
        })
    })
}

const _getCategoryListData = (categories) => {
    return new Promise(async (resolve, reject) => {
        const categoriesData = []
        try {
            await Promise.all(categories.map(async (ref) => {
                const data = await _getCategoryData(ref)
                categoriesData.push(data)
            }))
            resolve(categoriesData)
        } catch (e) {
            reject(e)
        }
    })
}

export const _requestGetComic = () => ({
    type: 'REQUEST_GET_COMIC'
})
export const _successGetComic = (payload) => ({
    type: 'SUCCESS_GET_COMIC',
    payload
})
export const _failedGetComic = (err) => ({
    type: 'FAILED_GET_COMIC',
    err
})

// -------------------------------------- Add comic ------------------------------------- //
export const addComic = (comic) => async (dispatch) => {
    const result = await checkIsTitleExist(comic.title)

    if (!result) {
        dispatch(_requestAddComic())
        let comicRef = firestore.collection('comics')
        let authorRef = firestore.collection('authors')
        let categoriesRef = firestore.collection('categories')
        let coverImageUrl = await uploadFile(comic.coverImage)
        const secondCoverImageUrl = await uploadFile(comic.secondCoverImage)

        let categories = []
        let categoriesObject = {}
        await Promise.all(comic.categories.map((val, key) => {
            categories.push(categoriesRef.doc(val))
            categoriesObject = {
                ...categoriesObject,
                [val]: true
            }
        }))

        comicRef.add({}).then((doc) => {
            comicRef.doc(doc.id).set({
                id: doc.id,
                title: comic.title,
                description: comic.description,
                coverImage: coverImageUrl,
                secondCoverImage: secondCoverImageUrl,
                author: authorRef.doc(comic.author),
                categories: categories,
                categoriesObject: categoriesObject,
                chapters: []
            })
        }).then(() => dispatch(_successAddComic())).then(() => message.success('Add new comic successfully.'))
            .catch((e) => dispatch(_failedAddComic()))
    }
}

export const _requestAddComic = () => ({
    type: 'REQUEST_ADD_COMIC'
})
export const _successAddComic = () => ({
    type: 'SUCCESS_ADD_COMIC'
})
export const _failedAddComic = (err) => ({
    type: 'FAILED_ADD_COMIC',
    err
})

// -------------------------------------- Delete comic ------------------------------------- //
export const deleteComic = (comic) => async (dispatch) => {
    dispatch(_requestDeleteComic())
    let comicsRef = firestore.collection('comics').doc(comic.id)
    let file = storage.refFromURL(comic.coverImage)
    file.delete()
        .then(() => comicsRef.delete()
            .then(() => dispatch(_successDeleteComic()))
            .then(() => message.success('Delete comic successfully'))
            .catch((e) => dispatch(_failedDeleteComic(e))))
        .catch((e) => dispatch(_failedDeleteComic(e)))
}

export const _requestDeleteComic = () => ({
    type: 'REQUEST_DELETE_COMIC'
})
export const _successDeleteComic = () => ({
    type: 'SUCCESS_DELETE_COMIC'
})
export const _failedDeleteComic = (err) => ({
    type: 'FAILED_DELETE_COMIC',
    err
})

// -------------------------------------- Update Comic Data ------------------------------------- //
export const updateComic = (comic, comicData) => async (dispatch) => {
    const comicRef = firestore.collection('comics').doc(comic.id)
    const authorRef = firestore.collection('authors')
    const categoriesRef = firestore.collection('categories')

    let categories = []
    let categoriesObject = {}
    await Promise.all(comicData.categories.map((val, key) => {
        categories.push(categoriesRef.doc(val))
        categoriesObject = {
            ...categoriesObject,
            [val]: true
        }
    }))
    console.log('Categories obj; ', categoriesObject)
    comicRef.set({
        title: comicData.title,
        description: comicData.description,
        author: authorRef.doc(comicData.author),
        categories: categories,
        categoriesObject: categoriesObject
    }, { merge: true }).then(
        comicRef.update({
            categoriesObject: categoriesObject
        }).then(() => dispatch(_successUpdateComic()))
            .then(() => message.success('Success update comic details'))
            .catch((e) => dispatch(_failedUpdateComic(e))))
        .catch((e) => dispatch(_failedUpdateComic(e)))
}

export const updateComicCover = (comic, imageFile) => async (dispatch) => {
    dispatch(_requestUpdateComic())
    const comicRef = firestore.collection('comics').doc(comic.id)
    let file = storage.refFromURL(comic.coverImage)
    let fileUrl = await uploadFile(imageFile)
    file.delete()
        .then(() => comicRef.set({
            coverImage: fileUrl
        }, { merge: true })
            .then(() => dispatch(_successUpdateComic()))
            .then(() => message.success('Update cover image successfully'))
            .catch((e) => dispatch(_failedUpdateComic(e)))
        )
        .catch((e) => dispatch(_failedUpdateComic(e)))
}

export const updateComicSecondCover = (comic, imageFile) => async (dispatch) => {
    dispatch(_requestUpdateComic())
    const comicRef = firestore.collection('comics').doc(comic.id)
    let file = storage.refFromURL(comic.secondCoverImage)
    let fileUrl = await uploadFile(imageFile)
    file.delete()
        .then(() => comicRef.set({
            secondCoverImage: fileUrl
        }, { merge: true })
            .then(() => dispatch(_successUpdateComic()))
            .then(() => message.success('Update cover image successfully'))
            .catch((e) => dispatch(_failedUpdateComic(e)))
        )
        .catch((e) => dispatch(_failedUpdateComic(e)))
}

export const _requestUpdateComic = () => ({
    type: 'REQUEST_UPDATE_COMIC'
})
export const _successUpdateComic = () => ({
    type: 'SUCCESS_UPDATE_COMIC'
})
export const _failedUpdateComic = (err) => ({
    type: 'FAILED_UPDATE_COMIC',
    err
})


// -------------------------------------- Add Chapter ------------------------------------- //
export const addChapter = (comicData, chapterData) => async (dispatch) => {
    const comicRef = firestore.collection('comics').doc(comicData.id)
    const chaptersRef = firestore.collection('chapters')

    try {
        dispatch(_requestAddChapter())
        let comic
        await comicRef.get().then((val) => {
            comic = val.data()
        }).catch((e) => console.log('Failed get comic data: ', e))

        const checkIsExist = await checkIsChapterExist(comic, chapterData)

        if (checkIsExist) {
            dispatch(_failedAddChapter('Chapter already exist in this comic!'))
        } else {
            let imageList = []
            await Promise.all(chapterData.image.map(async (img, key) => {
                const imageUrl = await uploadFile(img.originFileObj)
                const image = {
                    imageUrl: imageUrl,
                    number: (key + 1)
                }
                imageList.push(image)
            }))

            let chapterId
            chaptersRef.add({}).then((doc) => {
                chapterId = doc.id
                chaptersRef.doc(doc.id).set({
                    id: doc.id,
                    image: imageList
                }).then(() => {
                    let chapters = []
                    if (comic.chapters) {
                        chapters = comic.chapters
                    }
                    chapters.push(
                        {
                            number: chapterData.number,
                            chapter: chaptersRef.doc(chapterId),
                            price: chapterData.price,
                            enable: true,
                        }
                    )
                    comicRef.set({
                        chapters: chapters
                    }, { merge: true }).then(() => dispatch(_successAddChapter()))
                        .then(() => message.success('Add new chapter successfully.'))
                        .then(() => console.log('Set chapter to comic success.'))
                        .catch((e) => console.log('Failed to add new chapter: ', e))

                })
                    .catch((e) => (console.log('Failed add chapter: ', e)))
            })
        }

    } catch (e) {
        dispatch(_failedAddChapter(e))
    }
}

const checkIsChapterExist = (comic, chapter) => {
    return new Promise(async (resolve, reject) => {
        let exist = false
        if (comic.chapters) {
            await Promise.all(comic.chapters.map((val) => {
                if (val.number === chapter.number) {
                    exist = true
                }
            }))
            if (exist === true) {
                resolve(true)
                message.error('This chapter has already exist in this comic!')
            } else if (exist === false) {
                resolve(false)
            }
        } else {
            resolve(false)
        }
    })
}

export const _requestAddChapter = () => ({
    type: 'REQUEST_ADD_CHAPTER'
})
export const _successAddChapter = () => ({
    type: 'SUCCESS_ADD_CHAPTER'
})
export const _failedAddChapter = (err) => ({
    type: 'FAILED_ADD_CHAPTER',
    err
})

