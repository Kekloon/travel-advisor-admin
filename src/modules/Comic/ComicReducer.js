const initialState = {
    isLoading: false,
    errorMessage: '',
    comics: [],
    success: false,
    comic: {},
    isUpdateLoading: false
}

export default function(state = initialState, action) {
    switch (action.type) {
        case 'REQUEST_GET_COMIC':
            return Object.assign({}, state, {
                isLoading: true
            })
        case 'SUCCESS_GET_COMIC':
            return Object.assign({}, state, {
                isLoading: false,
                comics: action.payload
            })
        case 'FAILED_GET_COMIC':
            return Object.assign({}, state, {
                isLoading: false,
                errorMessage: action.err
            })
        case 'REQUEST_ADD_COMIC':
            return Object.assign({}, state, {
                isLoading: true,
                success: false
            })
        case 'SUCCESS_ADD_COMIC':
            return Object.assign({}, state, {
                isLoading: false,
                success: true
            })
        case 'FAILED_ADD_COMIC':
            return Object.assign({}, state, {
                isLoading: false,
                success: false
            })
        case 'REQUEST_DELETE_COMIC':
            return Object.assign({}, state, {
                isLoading: true
            })
        case 'SUCCESS_DELETE_COMIC':
            return Object.assign({}, state, {
                isLoading: false
            })
        case 'FAILED_DELETE_COMIC':
            return Object.assign({}, state, {
                isLoading: false
            })
        case 'REQUEST_ADD_CHAPTER':
            return Object.assign({}, state, {
                isUpdateLoading: true,
                success: false
            })
        case 'SUCCESS_ADD_CHAPTER':
            return Object.assign({}, state, {
                isUpdateLoading: false,
                success: true
            })
        case 'FAILED_ADD_CHAPTER':
            return Object.assign({}, state, {
                isUpdateLoading: false,
                success: false
            })
        case 'REQUEST_UPDATE_COMIC':
            return Object.assign({}, state, {
                isUpdateLoading: true
            })
        case 'SUCCESS_UPDATE_COMIC':
            return Object.assign({}, state, {
                isUpdateLoading: false
            })
        case 'FAILED_UPDATE_CATEGORY':
            return Object.assign({}, state, {
                isUpdateLoading: false
            })
        case 'SET_COMIC':
            return Object.assign({}, state, {
                comic: action.payload,
                isLoading: false
            })
        default:
            return state
    }
}
