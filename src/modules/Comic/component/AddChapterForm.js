import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Button, InputNumber, Upload, Icon, message, Spin } from 'antd'
const FormItem = Form.Item
const Dragger = Upload.Dragger

class AddChapterComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            comic: {
                chapters: []
            }
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.removeAllImage = this.removeAllImage.bind(this)
    }

    componentWillMount() {
        this.setState({
            comic: this.props.comic
        })
    }

    normFile = (e) => {
        if (e.fileList.length) {
            if (Array.isArray(e)) {
                return e;
            }
            return e && e.fileList;
        }
    }

    removeAllImage() {
        const { form } = this.props
        form.setFieldsValue({
            upload: []
        })
    }

    handleSubmit(e) {
        e.preventDefault()
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                const chapter = {
                    number: values.chapterNumber,
                    image: values.upload,
                    price: values.price
                }
                console.log(chapter)
                this.props.addChapter(this.state.comic, chapter)
                this.removeAllImage()
            }
        })
    }

    render() {
        const { comic } = this.state
        const { getFieldDecorator } = this.props.form
        const formItemLayout = {
            labelCol: { span: 4 },
            wrapperCol: { span: 16 },
        }

        const uploadProps = {
            name: 'file',
            multiple: true,
            onChange(info) {
                const status = info.file.status;
                if (status !== 'uploading') {
                    // console.log(info.file, info.fileList);
                }
                if (status === 'done') {
                    // message.success(`${info.file.name} file uploaded successfully.`)
                } else if (status === 'error') {
                    message.error(`${info.file.name} file upload failed.`)
                }
            }
        }
        return (
            <Spin spinning={this.props.isUpdateLoading} >
                <Form onSubmit={this.handleSubmit} >
                    <FormItem label='Chapter' {...formItemLayout} >
                        {
                            comic.chapters ?
                                getFieldDecorator('chapterNumber', { initialValue: comic.chapters.length, rules: [{ required: true, message: 'Please input chapter number' }] })(
                                    <InputNumber min={1} max={2000} />
                                )
                                :
                                getFieldDecorator('chapterNumber', { initialValue: 1, rules: [{ required: true, message: 'Please input chapter number' }] })(
                                    <InputNumber min={1} max={2000} />
                                )
                        }
                    </FormItem>
                    <FormItem label='Price' {...formItemLayout}>
                        {
                            getFieldDecorator('price', { initialValue: 0, rules: [{ required: true, message: 'Please key in price' }] })(
                                <InputNumber />
                            )
                        }
                    </FormItem>
                    <FormItem label='Pages Image'>
                        {getFieldDecorator('upload', { valuePropName: 'fileList', getValueFromEvent: this.normFile, rules: [{ required: true }] })
                            (
                            <Dragger {...uploadProps} >
                                <p className='ant-upload-drag-icon'>
                                    <Icon type='inbox' />
                                </p>
                                <p className='ant-upload-text'>Click or drag file to this area to upload</p>
                                <p className='ant-upload-hint'>Support for a single or bulk upload. Strictly prohibit from uploading company data or other band files</p>
                            </Dragger>
                            )}
                    </FormItem>
                    <FormItem>
                        <Button type='primary' htmlType='submit' >Add</Button>
                        <Button onClick={() => this.removeAllImage()} >Clear</Button>
                    </FormItem>
                </Form >
            </Spin>
        )
    }
}

AddChapterComponent.propTypes = {
    form: PropTypes.object,
    comic: PropTypes.object,
    addChapter: PropTypes.func,
    isUpdateLoading: PropTypes.bool
}

const WrapperAddChapterComponent = Form.create()(AddChapterComponent)
export default WrapperAddChapterComponent