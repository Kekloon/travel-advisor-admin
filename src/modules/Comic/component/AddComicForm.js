import React, { Component } from 'react'
import { Form, Spin, Input, Button, Upload, Icon, Select } from 'antd'
import PropTypes from 'prop-types'
const FormItem = Form.Item
const Option = Select.Option

class AddComicForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
            comic: {},
            authorList: [],
            categoryList: []
        }
    }

    componentWillMount() {
        this.props.getAuthorList()
        this.props.getCategories()
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isLoading: nextProps.isLoading,
            authorList: nextProps.authorList,
            categoryList: nextProps.categoryList
        })
    }

    handleSubmit = (e) => {
        e.preventDefault()
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                this.setState({
                    comic: {
                        title: values.title,
                        description: values.description,
                        coverImage: values.upload[0].originFileObj,
                        secondCoverImage: values.upload1[0].originFileObj,
                        author: values.author,
                        categories: values.categories,
                        chapter: []
                    }
                }, () => this.props.addComic(this.state.comic))
            }
        })
    }

    normFile = (e) => {
        if (e.fileList.length <= 1) {
            if (Array.isArray(e)) {
                return e
            }
            return e && e.fileList
        }
    }

    render() {
        const { isLoading, authorList, categoryList } = this.state
        const { getFieldDecorator } = this.props.form

        return (
            <Spin spinning={isLoading}>
                <Form onSubmit={this.handleSubmit}>
                    <FormItem label='Title' >
                        {getFieldDecorator('title', { rules: [{ required: true, message: 'Please input comic title' }] })(<Input />)}
                    </FormItem>
                    <FormItem label='Description' >
                        {getFieldDecorator('description', { rules: [{ required: true, message: 'Please input comic title' }] })(<Input.TextArea />)}
                    </FormItem>
                    <FormItem label='Author'>
                        {getFieldDecorator('author', { rules: [{ required: true, message: 'Please input author' }] })
                            (
                            <Select showSearch placeholder='Please input author'
                                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                            >
                                {authorList.map((val, key) => {
                                    return <Option key={key} title={val.name} value={val.id}>{val.name}</Option>
                                })}
                            </Select>
                            )}
                    </FormItem>
                    <FormItem label='Select category [multiple]'>
                        {getFieldDecorator('categories', { rules: [{ required: true, message: 'Please select comic' + "'" + 's category!', type: 'array' },], })
                            (
                            <Select mode='multiple' placeholder='Please select category' optionFilterProp='title'>
                                {categoryList.map((val, key) => {
                                    return <Option key={key} title={val.name} value={val.id}>{val.name}</Option>
                                })}
                            </Select>
                            )}
                    </FormItem>
                    <FormItem label='Cover Image ( Portrait )' >
                        {getFieldDecorator('upload', { valuePropName: 'fileList', getValueFromEvent: this.normFile, rules: [{ required: true }] })
                            (
                            <Upload name='logo' action='' listType='picture'>
                                <Button>
                                    <Icon type='upload' /> Click to upload
                                </Button>
                            </Upload>
                            )}
                    </FormItem>
                    <FormItem label='Cover Image ( Landscape )' >
                        {getFieldDecorator('upload1', { valuePropName: 'fileList', getValueFromEvent: this.normFile, rules: [{ required: true }] })
                            (
                            <Upload name='logo' action='' listType='picture'>
                                <Button>
                                    <Icon type='upload' /> Click to upload
                                </Button>
                            </Upload>
                            )}
                    </FormItem>
                    <FormItem>
                        <Button type='primary' htmlType='submit' >Add</Button>
                    </FormItem>
                </Form>
            </Spin>
        )
    }
}

AddComicForm.propTypes = {
    getAuthorList: PropTypes.func,
    getCategories: PropTypes.func,
    isLoading: PropTypes.bool,
    authorList: PropTypes.array,
    categoryList: PropTypes.array,
    form: PropTypes.object,
    addComic: PropTypes.func
}

const WrappedAddComicForm = Form.create()(AddComicForm)
export default WrappedAddComicForm
