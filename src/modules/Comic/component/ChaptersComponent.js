import React, { Component } from 'react'
import PropTypes from 'prop-types'
import AddChapterForm from './AddChapterForm'
import { Button, Form, Row, Col, Icon, Modal, Spin } from 'antd'
const FormItem = Form.Item
const confirm = Modal.confirm

class ChaptersComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            modalVisible: false,
            comic: {},
            success: false
        }
    }

    componentWillMount() {
        this.setState({
            comic: this.props.comic
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            comic: nextProps.comic
        })
        return nextProps.success ? this.setModalVisible(false) : null
    }

    setModalVisible(bool) {
        this.setState({
            modalVisible: bool
        })
    }

    showModal() {
        return (
            <Modal
                title='Add new chapter'
                wrapClassName='vertical-center-modal'
                visible={this.state.modalVisible}
                onCancel={() => this.setModalVisible(false)}
                footer={[]}
            >
                <div>
                    {
                        <AddChapterForm
                            comic={this.state.comic}
                            addChapter={this.props.addChapter}
                            isUpdateLoading={this.props.isUpdateLoading}
                        />
                    }
                </div>
            </Modal>
        )
    }

    showDeleteConfirmation(chapter) {
        confirm({
            title: 'Are you sure delete?',
            content: 'Chapter ' + chapter.number,
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk: () => {
                // this.props.deleteCategory(item)
                console.log('Delete chapter ', chapter.chapter.id)
            },
            onCancel() { },
        })
    }

    getFields() {
        const { comic } = this.state
        const { getFieldDecorator } = this.props.form
        const children = []
        if (comic.chapters) {
            // if (comic.chapters.length > 0) {
                comic.chapters.map((val, key) => {
                    children.push(
                        <Col span={4} key={val.number} style={{ display: 'block' }}>
                            <FormItem style={{ display: 'inline-block' }}>
                                {getFieldDecorator(`field-${key}`)(
                                    <Button onClick={() => console.log(val.chapter.id)} >Chapter {val.number}</Button>
                                )}
                                <Icon
                                    style={{ marginLeft: '1em' }}
                                    className='dynamic-delete-button'
                                    type='minus-circle-o'
                                    onClick={() => this.showDeleteConfirmation(val)}
                                />
                            </FormItem>
                        </Col>
                    )
                })
                return children.sort((a, b) => a.key - b.key)
            // }
        }
    }

    render() {
        const { comic } = this.state
        const { isLoading } = this.props

        console.log('Render comics chapter: ', comic)

        const renderChapter = (
            <Form>
                <Row gutter={24}>{this.getFields()}</Row>
            </Form>
        )
        return (
            isLoading ? <Spin spinning={isLoading} style={{ width: '100%', marginBottom: '50%' }} /> :
                <div>
                    <div style={{ width: '100%', borderBottom: '1px solid black', marginBottom: '1em', overflow: 'hidden' }}>
                        <h6>Chapter list</h6>
                    </div>
                    {comic.chapters ?  <div> {renderChapter} </div> : <div>No Data</div> }
                    {this.showModal()}
                    <Button type='primary' onClick={() => this.setModalVisible(true)} style={{ float: 'right' }} >Add new chapter</Button>
                </div>
        )
    }
}

ChaptersComponent.propTypes = {
    form: PropTypes.object,
    comic: PropTypes.object,
    addChapter: PropTypes.func,
    isLoading: PropTypes.bool,
    success: PropTypes.bool,
    isUpdateLoading: PropTypes.bool
}

const WrapperChaptersComponent = Form.create()(ChaptersComponent)
export default WrapperChaptersComponent