import React, { Component } from 'react'
import PropType from 'prop-types'
import { List, Button, Modal } from 'antd'
import AddComicForm from '../container/AddComicFormContainer'
import { Link } from 'react-router-dom'

const confirm = Modal.confirm

class ComicList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: false,
      comicList: [],
      comic: {},
      // Pagination
      pageOfItems: [],
      currentPage: 0,
      pageSize: 4,
      // Modal
      modalVisible: false,
      modalType: 'Add',

    }
    this.onChangePage = this.onChangePage.bind(this)
    this.setModalVisible = this.setModalVisible.bind(this)
    this.showDeleteConfirm = this.showDeleteConfirm.bind(this)

    this.handleClick = this.handleClick.bind(this)
  }

  componentWillMount() {
    this.props.getComics()
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      isLoading: nextProps.isLoading,
      comicList: nextProps.comicList
    }, () => this.onChangePage(1, this.state.pageSize))
    return nextProps.success ? this.setModalVisible(false) : null
  }

  handleClick(comic) {
    this.props.setComic(comic)
  }

  onChangePage(page, pageSize) {
    let array = []

    this.state.comicList.map((val, key) => {
      if (key < (page * pageSize)) {
        if (key >= ((page - 1) * pageSize)) {
          array.push(val)
        }
      }
    })
    this.setState({
      pageOfItems: array,
      currentPage: page
    })
  }

  setModalVisible(bool, type, item) {
    this.setState({
      modalVisible: bool,
      modalType: type,
      comic: item
    })
  }

  showDeleteConfirm(item) {
    confirm({
      title: 'Are you sure delete this comic?',
      content: item.title,
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk: () => {
        this.props.deleteComic(item)
      },
      onCancel() { },
    })
  }

  showModal() {
    const { modalType } = this.state
    if (modalType === 'Add') {
      return (
        <Modal
          title='New comic'
          visible={this.state.modalVisible}
          onCancel={() => this.setModalVisible(false)}
          footer={[]}
        >
          <div>
            <AddComicForm />
          </div>
        </Modal>
      )
    }
  }

  render() {
    const { isLoading, comicList, pageOfItems, pageSize } = this.state
    const paginationProps = {
      total: comicList.length,
      pageSize: pageSize,
      defaultCurrent: 1,
      showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`,
      onChange: (page, pageSize) => this.onChangePage(page, pageSize),
      current: this.state.currentPage,
    }
    return (
      <div>
        <div style={{ width: '100%', display: 'table' }}>
          <h3 style={{ display: 'table-cell' }} >Comic List</h3>
          <div style={{ textAlign: 'right', display: 'table-cell' }}>
            <Button onClick={() => this.setModalVisible(true, 'Add')} type='primary'>Add new comic</Button>
          </div>
        </div>
        {this.showModal()}
        <List
          className='demo-loadmore-list'
          loading={isLoading}
          itemLayout='horizontal'
          pagination={paginationProps}
          dataSource={pageOfItems}
          renderItem={item => (
              <List.Item
                actions={[
                  <Link to='/comic/details'><Button onClick={() => this.handleClick(item)}>View</Button></Link>,
                  <Button onClick={() => this.showDeleteConfirm(item)}>Delete</Button>
                ]}>
                <List.Item.Meta
                  avatar={<img src={item.coverImage} height='150px' />}
                  title={item.title}
                  description={(
                    // Line break string
                    'Author: ' + item.author.name + '\n' +
                    'Categories: ' + item.categories.map((val, key) => {
                      if (key > 0) {
                        return ' ' + val.name
                      } else {
                        return val.name
                      }
                    }) + '\n' +
                    item.description).split('\n').map((i, key) => {
                      return <div key={key}>{i}</div>
                    }
                    )}
                />
              </List.Item>
          )}
        />
      </div >
    )
  }
}

ComicList.propTypes = {
  getComics: PropType.func,
  setComic: PropType.func,
  isLoading: PropType.bool,
  comicList: PropType.array,
  success: PropType.bool,
  deleteComic: PropType.func
}

export default ComicList
