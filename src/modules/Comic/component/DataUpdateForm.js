import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Input, Button, Select, message, Spin } from 'antd'
const Option = Select.Option
const FormItem = Form.Item

class DataUpdateForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            comic: {},
            authorList: [],
            categoryList: []
        }
    }

    componentWillMount() {
        this.setState({
            comic: this.props.comic,
            authorList: this.props.authorList,
            categoryList: this.props.categoryList
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            comic: nextProps.comic,
            authorList: nextProps.authorList,
            categoryList: nextProps.categoryList
        })
    }

    handleDataUpdate = (e) => {
        const { comic } = this.state
        let samCount = 0
        e.preventDefault()
        this.props.form.validateFields(async (err, values) => {
            if (!err) {
                if (values.title === comic.title && values.description === comic.description && values.author === comic.author.id) {
                    comic.categories.map((obj) => {
                        values.categories.map((val) => {
                            if (obj.id === val) {
                                samCount++
                            }
                        })
                    })

                    if (comic.categories.length === samCount && comic.categories.length === values.categories.length) {
                        this.setState({}, () => message.error('Its seems like same data'))
                    } else {
                        this.props.updateComic(comic, values)
                    }
                } else {
                    this.props.updateComic(comic, values)
                }
            }
        })
    }

    arrayObjectToString(categories) {
        const stringArray = []
        categories.forEach((obj, key) => {
            stringArray.push(obj.id)
        })
        return stringArray
    }

    render() {
        const { comic, authorList, categoryList } = this.state
        const { getFieldDecorator } = this.props.form
        const textAreaAutoSize = true

        const formItemLayout = {
            labelCol: { xs: { span: 24 }, sm: { span: 3 }, },
            wrapperCol: { xs: { span: 24 }, sm: { span: 16 }, },
        }

        return (
            <Form onSubmit={this.handleDataUpdate} style={{ float: 'left', width: '100%', margin: '2em' }} >
                <Spin spinning={this.props.isUpdateLoading}>
                    <FormItem {...formItemLayout} label='Title' style={{ marginBottom: '0' }}>
                        {getFieldDecorator('title',
                            {
                                initialValue: comic.title,
                                rules: [{ required: true, message: 'Please input title!' }],
                            }
                        )(
                            <Input />
                        )}
                    </FormItem>

                    <FormItem {...formItemLayout} label='Description' style={{ marginBottom: '0' }}>
                        {getFieldDecorator('description', { initialValue: comic.description })(<Input.TextArea autosize={textAreaAutoSize} />)}
                    </FormItem>

                    <FormItem {...formItemLayout} label='Author' style={{ marginBottom: '0' }}>
                        {
                            getFieldDecorator('author',
                                { initialValue: comic.author.id })(
                                    <Select showSearch placeholder='Please input author' optionFilterProp='title'>
                                        {
                                            authorList.map((obj, key) => {
                                                return <Option key={key} title={obj.name} value={obj.id} >{obj.name}</Option>
                                            })
                                        }
                                    </Select>
                                )
                        }
                    </FormItem>

                    <FormItem {...formItemLayout} label='Categories' style={{ marginBottom: '0' }}>
                        {
                            getFieldDecorator('categories',
                                {
                                    initialValue: this.arrayObjectToString(comic.categories),
                                    rules: [{ required: true, message: 'Please select comic' + "'" + 's category!', type: 'array' }]
                                })(
                                    <Select mode='multiple' placeholder='Please select category' optionFilterProp='title'>
                                        {
                                            categoryList.map((obj, key) => {
                                                return <Option key={key} title={obj.name} value={obj.id} >{obj.name}</Option>
                                            })
                                        }
                                    </Select>
                                )
                        }
                    </FormItem>

                    <FormItem {...formItemLayout} >
                        <Button type='primary' style={{ marginLeft: '19%' }} htmlType='submit' >Update</Button>
                    </FormItem>
                </Spin>
            </Form>
        )
    }
}

DataUpdateForm.propTypes = {
    form: PropTypes.object,
    comic: PropTypes.object,
    getAuthorList: PropTypes.func,
    getCategories: PropTypes.func,
    authorList: PropTypes.array,
    categoryList: PropTypes.array,
    isLoading: PropTypes.bool,
    isUpdateLoading: PropTypes.bool,
    updateComic: PropTypes.func
}

const WrappedDataUpdateForm = Form.create()(DataUpdateForm)
export default WrappedDataUpdateForm
