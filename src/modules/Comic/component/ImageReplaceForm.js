import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Upload, Button, Icon, Modal, Spin } from 'antd'
// import _ from 'lodash'
const FormItem = Form.Item

class ImageReplaceForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            comic: {},
            previewVisible: false,
            previewImage: '',
            normFileSize: 0
        }
        this.removeAllImage = this.removeAllImage.bind(this)
    }

    componentWillMount() {
        this.setState({
            comic: this.props.comic
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            comic: nextProps.comic
        })
    }

    handleCancel = () => this.setState({ previewVisible: false })
    handlePreview = (file) => {
        this.setState({
            previewImage: file.url || file.thumbUrl,
            previewVisible: true,
        })
    }

    normFile = (e) => {
        console.log(e.fileList)
        if (e.fileList.length <= 1) {
            this.setState({
                normFileSize: e.fileList.length
            })
            if (Array.isArray(e)) {
                return e
            }
            return e && e.fileList
        }
    }

    removeAllImage() {
        const { form } = this.props
        form.setFieldsValue({
            upload: []
        })
        this.setState({
            normFileSize: 0
        })
    }

    handleReplacCoverImage = (e) => {
        e.preventDefault()
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.updateComicCover(this.state.comic, values.upload[0].originFileObj)
                this.removeAllImage()
            }
        })
    }

    render() {
        const { comic } = this.state
        const { getFieldDecorator } = this.props.form

        // --- Component UI ---
        const uploadButton = (
            <div>
                <Icon type='plus' />
                <div className='ant-upload-text'>Upload</div>
            </div>
        )

        return (
            <Form onSubmit={this.handleReplacCoverImage} style={{ float: 'left', margin: '1em', width: this.props.type === 'secondCoverImage' ? '33em' : '18em' }}>
                <Spin spinning={this.props.isUpdateLoading}>
                    <div style={{ textAlign: 'center' }}>
                        {
                            this.props.type === 'secondCoverImage' ?
                                <span>Cover Image ( Landscape )</span>
                                :
                                <span>Cover Image</span>
                        }
                    </div>
                    <FormItem style={{ marginBottom: '0em' }}>
                        <div>
                            <div style={{ float: 'left' }}>
                                <Upload disabled listType='picture-card' >
                                    <img src={this.props.type === 'secondCoverImage' ? comic.secondCoverImage : comic.coverImage} height='150px' />
                                </Upload>
                            </div>
                            <Button type='primary' ghost htmlType='submit' style={{ width: '104px' }}>
                                Replace <Icon type='down' />
                            </Button>
                            {getFieldDecorator('upload', { valuePropName: 'fileList', getValueFromEvent: this.normFile, rules: [{ required: true }] }
                            )(<Upload
                                listType='picture-card'
                                onPreview={this.handlePreview}
                                onChange={this.handleChange}
                            >
                                {this.state.normFileSize >= 1 ? null : uploadButton}
                            </Upload>
                            )}
                            <Modal visible={this.state.previewVisible} footer={null} onCancel={this.handleCancel}>
                                <img alt='example' style={{ width: '100%' }} src={this.state.previewImage} />
                            </Modal>
                        </div>
                    </FormItem>
                </Spin>
            </Form>
        )
    }
}

ImageReplaceForm.propTypes = {
    form: PropTypes.object,
    comic: PropTypes.object,
    isUpdateLoading: PropTypes.bool,
    updateComicCover: PropTypes.func,
    type: PropTypes.string
}

const WrappedImageReplaceForm = Form.create()(ImageReplaceForm)
export default WrappedImageReplaceForm
