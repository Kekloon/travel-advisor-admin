import { connect } from 'react-redux'
import AddComicForm from '../component/AddComicForm'

import { getAuthorList } from '../../Author/AuthorAction'
import { getCategories } from '../../Category/CategoryAction'
import { addComic } from '../ComicAction'

const mapStateToProps = (state) => ({
    authorList: state.author.authorList,
    categoryList: state.category.categoryList,
    isLoading: state.author.isLoading || state.category.isLoading || state.comic.isLoading
})

const mapDispatchToProps = (dispatch) => ({
    getAuthorList: () => {
        dispatch(getAuthorList())
    },
    getCategories: () => {
        dispatch(getCategories())
    },
    addComic: (comic) => {
        dispatch(addComic(comic))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(AddComicForm)
