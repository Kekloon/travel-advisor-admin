import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
// Component
import ImageReplaceForm from '../component/ImageReplaceForm'
import DataUpdateForm from '../component/DataUpdateForm'
import ChapterComponent from '../component/ChaptersComponent'
// Action
import { getCategories } from '../../Category/CategoryAction'
import { getAuthorList } from '../../Author/AuthorAction'
import { addChapter, updateComic, updateComicCover, updateComicSecondCover } from '../ComicAction'
// UI
import { Button, Icon, Spin } from 'antd'

class ComicDetails extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: true,
            comic: {},
            authorList: [],
            categoryList: [],
            previewVisible: false,
            previewImage: '',
            normFileSize: 0,
            isUpdateLoading: false,
            success: false
        }
    }

    componentWillMount() {
        this.props.getCategories()
        this.props.getAuthorList()
    }

    componentWillReceiveProps(nextProps) {
        nextProps.comics.map((obj) => {
            if (obj.id === nextProps.comic.id) {
                this.setState({ comic: obj })
            }
        })
        this.setState({
            authorList: nextProps.authorList,
            categoryList: nextProps.categoryList,
            isLoading: nextProps.isLoading,
            isUpdateLoading: nextProps.isUpdateLoading,
            success: nextProps.success
        })
    }

    render() {
        const { isLoading, comic, authorList, categoryList } = this.state
        return (
            isLoading ? <Spin spinning={isLoading} style={{ width: '100%', marginBottom: '50%' }} />
                :
                <div>
                    <div style={{ textAlign: 'center', marginBottom: '1em' }}>
                        <Link to='/comicList'>
                            <Button type='primary' style={{ float: 'left' }}>
                                <Icon type='left' />Go back
                        </Button>
                        </Link>
                        <h3 style={{ width: '90%' }}>Comic Details</h3>
                    </div>

                    <DataUpdateForm
                        comic={comic}
                        authorList={authorList}
                        categoryList={categoryList}
                        isUpdateLoading={this.state.isUpdateLoading}
                        updateComic={this.props.updateComic}
                    />

                    <div style={{ marginLeft: '8em'  }}>
                        <ImageReplaceForm
                            type=''
                            comic={comic}
                            isUpdateLoading={this.state.isUpdateLoading}
                            updateComicCover={this.props.updateComicCover}
                        />

                        <ImageReplaceForm
                            type='secondCoverImage'
                            comic={comic}
                            isUpdateLoading={this.state.isUpdateLoading}
                            updateComicCover={this.props.updateComicSecondCover}
                        />
                    </div>

                    <ChapterComponent
                        comic={comic}
                        addChapter={this.props.addChapter}
                        isUpdateLoading={this.state.isUpdateLoading}
                        success={this.state.success}
                    />

                </div>
        )
    }
}

ComicDetails.propTypes = {
    comic: PropTypes.object,
    getAuthorList: PropTypes.func,
    getCategories: PropTypes.func,
    authorList: PropTypes.array,
    categoryList: PropTypes.array,
    isLoading: PropTypes.bool,
    comics: PropTypes.array,
    addChapter: PropTypes.func,
    isUpdateLoading: PropTypes.bool,
    success: PropTypes.bool,
    updateComic: PropTypes.func,
    updateComicCover: PropTypes.func,
    updateComicSecondCover: PropTypes.func
}

const mapStateToProps = (state) => ({
    isLoading: state.comic.isLoading || state.author.isLoading || state.category.isLoading,
    comic: state.comic.comic,
    comics: state.comic.comics,
    categoryList: state.category.categoryList,
    authorList: state.author.authorList,
    success: state.comic.success,
    isUpdateLoading: state.comic.isUpdateLoading
})
const mapDispatchToProps = (dispatch) => ({
    getCategories: () => {
        dispatch(getCategories())
    },
    getAuthorList: () => {
        dispatch(getAuthorList())
    },
    addChapter: (comic, chapter) => {
        dispatch(addChapter(comic, chapter))
    },
    updateComic: (comic, newData) => {
        dispatch(updateComic(comic, newData))
    },
    updateComicCover: (comic, imgFile) => {
        dispatch(updateComicCover(comic, imgFile))
    },
    updateComicSecondCover: (comic, imgFile) => {
        dispatch(updateComicSecondCover(comic, imgFile))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(ComicDetails)
