import { connect } from 'react-redux'
import ComicList from '../component/ComicList'
import { getComics, deleteComic, setComic } from '../ComicAction'

const mapStateToProps = (state) => ({
    isLoading: state.comic.isLoading,
    comicList: state.comic.comics,
    success: state.comic.success
})

const mapDispatchToProps = (dispatch) => ({
    getComics: () => {
        dispatch(getComics())
    },
    deleteComic: (comic) => {
        dispatch(deleteComic(comic))
    },
    setComic: (comic) => {
        dispatch(setComic(comic))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(ComicList)
