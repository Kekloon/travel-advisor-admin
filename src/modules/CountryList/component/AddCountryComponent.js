import React, { Component } from 'react'
import { Form, Input, Button, Upload, Icon, Spin } from 'antd'
import CountryListComponent from '../component/CountryListComponent'

const FormItem = Form.Item;

class AddCountryComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            country: {},
            isLoading: false,
            fileList: [],
            visible: false,
            resetFormData: false,
        }
        this.setResetFromData = this.setResetFromData.bind(this)

    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isLoading: nextProps.isLoading,
            resetFormData: nextProps.resetFormData,


        })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.setState({
                    country: {
                        name: values.CountryName,
                        coverImage: values.CountryCoverImage[0].originFileObj
                    }

                }, () => this.props.addNewCountry(this.state.country))
            }
        })



    }

    setResetFromData(bool) {
        if (bool === true) {
            this.props.form.resetFields();
            this.state.fileList = [];

        }

    }

    handleChange = ({ fileList }) => this.setState({ fileList })



    normFile = (e) => {
        if (e.fileList.length <= 1) {
            if (Array.isArray(e)) {
                return e;
            }
            return e && e.fileList;
        }
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const { formLayout, fileList } = this.state;
        // const { setModalVisible } = this.props.setModalVisible;
        const uploadButton = (
            <div>
                <Icon type='plus' />
                <div className='ant-upload-text'>Upload</div>
            </div>
        );
        const formItemLayout = formLayout === 'horizontal' ? {
            labelCol: { span: 4 },
            wrapperCol: { span: 14 },
        } : null;
        const buttonItemLayout = formLayout === 'horizontal' ? {
            wrapperCol: { span: 14, offset: 4 },
        } : null;
        return (
            <Spin spinning={this.state.isLoading}>
                <Form onSubmit={this.handleSubmit}
                    onChange={this.setResetFromData(this.state.resetFormData)}
                >
                    <FormItem label='Country name'
                        {...formItemLayout}
                    >
                        {getFieldDecorator('CountryName', {
                            rules: [{ required: true, message: 'Please input country name!' }],
                        })(
                            <Input prefix={<Icon type='dribbble' theme='outlined' style={{ color: 'rgba(0,0,0,.25)' }} />}
                                placeholder='Country name' />
                        )}
                    </FormItem>
                    <FormItem label='Country Cover Image'
                        {...formItemLayout}
                    >
                        {getFieldDecorator('CountryCoverImage', {
                            valuePropName: 'fileList',
                            getValueFromEvent: this.normFile,
                            onChange: this.handleChange,
                            rules: [{ required: true }],
                        })(
                            <Upload action='' listType='picture-card'
                            // file={file}
                            // onChange={this.handleChange}
                            >
                                {fileList.length >= 1 ? null : uploadButton}
                            </Upload>
                        )}
                    </FormItem>
                    <FormItem {...buttonItemLayout}>
                        <Button className='NewButton' htmlType='submit'>Add</Button>
                    </FormItem>
                </Form>
            </Spin>

        )
    }
}

const WrappedAddCountryForm = Form.create()(AddCountryComponent)
export default WrappedAddCountryForm