import React, { Component } from 'react'
import AddCountryContainer from '../container/AddCountryContainer'
import EditCountryContainer from '../container/EditCountryContainer'
import AddCountryComponent from '../component/AddCountryComponent'

import { List, Button, Modal } from 'antd';
const confirm = Modal.confirm;

class CountryList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
            countryList: [],
            modalVisible: false,
            modalType: 'AddNewCountry',
            countryData: null
        }
        this.setModalVisible = this.setModalVisible.bind(this)
        this.showDeleteConfirm = this.showDeleteConfirm.bind(this)
    }

    componentWillMount() {
        this.props.getCountryList()
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isLoading: nextProps.isLoading,
            countryList: nextProps.countryList,
            modalVisible: nextProps.modalVisible,
        })

        nextProps.success ? this.setModalVisible(false) : null
        // nextProps.countryList != null ? this.setModalVisible(false) : this.setModalVisible(true, 'AddNewCountry', null)
    }

    setModalVisible(bool, type, data) {
        this.setState({
            modalVisible: bool,
            modalType: type,
            countryData: data,
        })
    }

    showModal() {
        const { modalType, countryData, isLoading } = this.state
        if (modalType === 'AddNewCountry') {
            return (
                <Modal
                    title='New Country'
                    centered
                    visible={this.state.modalVisible}
                    onCancel={() => this.setModalVisible(false)}
                    footer={[]}

                >
                    <AddCountryContainer setModalVisible={this.setModalVisible} />
                </Modal>
            )
        }
        else if (modalType === 'Edit') {
            return (
                <Modal
                    title='Edit Country'
                    centered
                    visible={this.state.modalVisible}
                    onCancel={() => this.setModalVisible(false)}
                    footer={[]}

                >
                    <EditCountryContainer data={countryData} />
                </Modal>
            )
        }
    }

    showDeleteConfirm(data) {
        confirm({
            title: 'Are you sure delete this country?',
            content: data.name,
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk: () => {
                this.props.deleteCountry(data)
            },
            onCancel() { },
        })
    }


    render() {
        const { isLoading, countryList, modalVisible } = this.state
        const loadMore = this.showModal()
        return (
            <div>
                <div style={{ width: '100%', display: 'table' }}>
                    <span className='title-text'>Country List</span>
                    <div style={{ textAlign: 'right', display: 'table-cell' }}>
                        <Button className='NewButton' onClick={() => this.setModalVisible(true, 'AddNewCountry')}> Add new Country</Button>
                    </div>
                </div>
                <List
                    className='demo-loadmore-list'
                    loading={isLoading}
                    itemLayout='horizontal'
                    loadMore={loadMore}
                    dataSource={countryList}
                    renderItem={data => (
                        <List.Item
                            actions={[
                                <Button onClick={() => this.setModalVisible(true, 'Edit', data)}>Edit</Button>,
                                <Button onClick={() => this.showDeleteConfirm(data)}>Delete</Button>
                            ]}
                        >
                            <List.Item.Meta
                                avatar={<img src={data.coverImage} height='90px' />}
                                title={data.name}
                                description='The ratio will be resize in user mobile application.'
                            />
                        </List.Item>
                    )}
                />
            </div>
        )
    }

}
export default CountryList