import React, { Component } from 'react'
import { Form, Input, Button, Upload, Icon, Spin } from 'antd';
const FormItem = Form.Item;

class EditCountryComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            country: {},
            isLoading: false,
            fileList: [],
            resetFormData:false,

        }
    }


    componentWillReceiveProps(nextProps) {
        this.setState({
            isLoading: nextProps.isLoading,
            resetFormData:nextProps.resetFormData,
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err,values)=>{
            if(!err){
                if(values.CountryCoverImage){
                    this.setState({
                        country:{
                            countryId:this.props.data.countryId,
                            name: values.countryName,
                            coverImage:values.CountryCoverImage[0].originFileObj,
                            LastCoverImage:this.props.data.coverImage,        
                        }
                    },()=>this.props.updateCountry(this.state.country))
                }
                else{
                    this.setState({
                        country:{
                            countryId:this.props.data.countryId,
                            name:values.countryName,
                        }
                    },()=>this.props.updateCountry(this.state.country))
                }
            }
        })

    }


    setResetFromData(bool) {
        if (bool === true) {
            this.props.form.resetFields();
            this.state.fileList = [];

        }

    }

    handleChange = ({ fileList }) => this.setState({ fileList })


    normFile = (e) => {
        if (e.fileList.length <= 1) {
            if (Array.isArray(e)) {
                return e;
            }
            this.setState({
                file: e.file
            })
            return e && e.fileList
        }
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const { formLayout, fileList } = this.state;
        // const { setModalVisible } = this.props.setModalVisible;
        const uploadButton = (
            <div>
                <Icon type='plus' />
                <div className='ant-upload-text'>Upload</div>
            </div>
        );
        const formItemLayout = formLayout === 'horizontal' ? {
            labelCol: { span: 4 },
            wrapperCol: { span: 14 },
        } : null;
        const buttonItemLayout = formLayout === 'horizontal' ? {
            wrapperCol: { span: 14, offset: 4 },
        } : null;

        return (
            <Spin spinning={this.state.isLoading}>
                <Form onSubmit={this.handleSubmit}
                    onChange={this.setResetFromData(this.state.resetFormData)}
                >
                    <FormItem label='Country name'
                        {...formItemLayout}
                    > 
                        {getFieldDecorator('countryName', {
                            rules: [{ required: true, message: 'Please input country name!' }],
                            initialValue: this.props.data.name
                        })(
                            <Input prefix={<Icon type='dribbble' theme='outlined' style={{ color: 'rgba(0,0,0,.25)' }} />}
                                placeholder='Country name' />
                        )}
                    </FormItem>
                    <FormItem label='Country Cover Image'
                        {...formItemLayout}
                    >
                        {getFieldDecorator('CountryCoverImage', {
                            valuePropName: 'fileList',
                            getValueFromEvent: this.normFile,
                            onChange: this.handleChange,
                        })(
                            <Upload action='' listType='picture-card'
                            >
                                {fileList.length >= 1 ? null : uploadButton}
                            </Upload>
                        )}
                    </FormItem>
                    <FormItem {...buttonItemLayout}>
                        <Button className='NewButton' htmlType='submit'>Edit</Button>
                    </FormItem>
                </Form>
            </Spin>

        )
    }
}

const WrappedEditCountryForm =Form.create()(EditCountryComponent);
export default WrappedEditCountryForm