import { connect } from 'react-redux'
import AddCountryComponent from '../component/AddCountryComponent'
import { addNewCountry } from '../countryListAction'

const mapStateToProps = (state) => ({
    isLoading: state.countryListReducer.isLoading,
    modalVisible: state.countryListReducer.modalVisible,
    resetFormData:state.countryListReducer.resetFormData,

})

const mapDispatchToProps = (dispatch) => ({
    addNewCountry: (country) => {
        dispatch(addNewCountry(country))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(AddCountryComponent)