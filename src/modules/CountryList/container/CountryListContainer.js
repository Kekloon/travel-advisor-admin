import { connect } from 'react-redux'
import CountryListComponent from '../component/CountryListComponent'
import { getCountryList, deleteCountry } from '../countryListAction'

const mapStateToProps = (state) => ({
    isLoading: state.countryListReducer.isLoading,
    countryList: state.countryListReducer.countryList,
    success: state.countryListReducer.success
})

const mapDispatchToProps = (dispatch) => ({
    getCountryList:()=>{
        dispatch(getCountryList())
    },
    deleteCountry:(fileUrl)=>{
        dispatch(deleteCountry(fileUrl))
    }
})

export default connect(mapStateToProps,mapDispatchToProps)(CountryListComponent)