import { connect } from 'react-redux'
import EditCountryComponent from '../component/EditCountryComponent'
import { updateCountry } from '../countryListAction'


const mapStateToProps = (state) => ({
    isLoading: state.countryListReducer.isLoading,
    modalVisible: state.countryListReducer.modalVisible,
    resetFormData:state.countryListReducer.resetFormData,
})

const mapDispatchToProps = (dispatch) => ({
    updateCountry: (country) => {
        dispatch(updateCountry(country))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(EditCountryComponent)