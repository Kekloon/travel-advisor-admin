import { storage, firestore } from '../../config/firebase'
import { uploadFile } from '../../config/helper'
import { message } from 'antd'
import { __await } from 'tslib';
import Item from 'antd/lib/list/Item';

// -------------------- check country name exis ---------------------//
const checkIsNameExist = (countryName) => {
    return new Promise((resolve, reject) => {
        let query = firestore.collection('countryList').where('name', '==', countryName)
        query.get().then((snapshot => {
            if (snapshot.docs.length > 0) {
                resolve(true)
                message.error('This country name was exsited !! ')
            }
            else {
                resolve(false)

            }
        })).catch((e) => reject(e))
    })
}

// ------------------- get country List -------------------------------//
export const getCountryList = () => async (dispatch) => {
    try {
        dispatch(_requestGetCountryList())
        firestore.collection('countryList').onSnapshot((val) => {
            firestore.collection("countryList").get().then((querySnapshot) => {
                let data = []
                querySnapshot.forEach((doc) => {
                    data.push(doc.data())
                })
                dispatch(_sucessGetCountryList(data))
            }).catch((e) => dispatch(_failGetCountryList(e)))
        })

    } catch (e) {
        dispatch(_failGetCountryList(e))
    }
}

export const _requestGetCountryList = () => ({
    type: 'REQUEST_GET_COUNTRYLIST'
})

export const _sucessGetCountryList = (payload) => ({
    type: 'SUCCESS_GET_COUNTRYLIST',
    payload
})

export const _failGetCountryList = (err) => ({
    type: 'FAILED_GET_CPUNTRYLIST',
    err
})

// -------------------------delete country ---------------------- //

export const deleteCountry = (data) => async (dispatch) => {
    dispatch(_requestDeleteCountryList())

    let countryListRef = firestore.collection('countryList').doc(data.countryId)
    let file = storage.refFromURL(data.coverImage)
    file.delete()
        .then(() => countryListRef.delete()
            .then(() => dispatch(_sucessDeleteCountryList()))
            .then(() => message.success('Delete Country List successfully'))
            .catch((e) => dispatch(_failDeleteCountryList(e)))
        ).catch((e) => dispatch(_failDeleteCountryList(e)))
}

export const _requestDeleteCountryList = () => ({
    type: 'REQUEST_DELETE_COUNTRY'
})

export const _sucessDeleteCountryList = () => ({
    type: 'SUCCESS_DELETE_COUNTRY'
})

export const _failDeleteCountryList = (err) => ({
    type: 'FAILED_DELETE_CATEGORY',
    err
})

// -----------------------add new country ------------------//4
export const addNewCountry = (country) => async (dispatch) => {
    const checkCountryName = await checkIsNameExist(country.name)

    if (!checkCountryName) {
        dispatch(_requestAddNewCountry())
        let countryRef = firestore.collection('countryList')
        let fileUrl = await uploadFile(country.coverImage)

        countryRef.add({}).then((doc) => {
            countryRef.doc(doc.id).set({
                countryId: doc.id,
                name: country.name,
                coverImage: fileUrl
            })
        }).then(() => dispatch(_successAddNewCountry())).then(() => message.success('Add new country sucessfully'))
            .catch((e) => dispatch(_failedAddNewCountry(e)))
    }
}

export const _requestAddNewCountry = () => ({
    type: 'REQUEST_ADD_NEW_COUNTRY'
})

export const _successAddNewCountry = () => ({
    type: 'SUCCESS_ADD_NEW_COUNTRY'
})

export const _failedAddNewCountry = (err) => ({
    type: 'FAILED_ADD_NEW_CATEGORY',
    err
})

// ----------------------- Edit country ------------------------//

export const updateCountry = (country) => async (dispatch) => {
    dispatch(_requestUpdateCountry())
    console.log(country.name)
    const ref = firestore.collection('countryList').doc(country.countryId)
    if (country.coverImage) {
        let file = storage.refFromURL(country.LastCoverImage)
        let fileUrl = await uploadFile(country.coverImage)
        await file.delete()
        ref.set({
                name: country.name,
                coverImage: fileUrl
            }, { merge: true })
                .then(() => dispatch(_successUpdateCountry()))
                .then(() => message.success('Update country successfully'))
                .catch((e) => dispatch(_failedUpdateCountry(e)))
            
    }
    else {
        ref.set({
            name: country.name,
        }, { merge: true })
            .then(() => dispatch(_successUpdateCountry()))
            .then(() => message.success('Update country successfully'))
            .catch((e) => dispatch(_failedUpdateCountry(e)))
    }
}

export const _requestUpdateCountry =()=>({
    type:'REQUEST_UPDATE_COUNTRY'
})

export const _successUpdateCountry =()=>({
    type:'SUCCESSS_UPDATE_COUNTRY'
})

export const _failedUpdateCountry =(err)=>({
    type:'FAILED_UPDATE_COUNTRY',
    err,
})