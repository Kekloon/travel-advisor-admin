const initialState = {
    isLoading: false,
    errorMessage: "",
    success: false,
    countryList: [],
    modalVisible:false,
    resetFormData:false,
}

export default function (state = initialState, action) {
    switch (action.type) {

        case 'REQUEST_GET_COUNTRYLIST':
            return Object.assign({}, state, {
                isLoading: true
            })
        case 'SUCCESS_GET_COUNTRYLIST':
            return Object.assign({}, state, {
                isLoading: false,
                countryList: action.payload
            })
        case 'FAILED_GET_CPUNTRYLIST':
            return Object.assign({}, state, {
                isLoading: false,
                errorMessage: action.err
            })
        case 'REQUEST_DELETE_COUNTRY':
            return Object.assign({}, state, {
                isLoading: true
            })
        case 'SUCCESS_DELETE_COUNTRY':
            return Object.assign({}, state, {
                isLoading: false
            })
        case 'FAILED_DELETE_CATEGORY':
            return Object.assign({}, state, {
                isLoading: false,
                errorMessage: action.err
            })
        case 'REQUEST_ADD_NEW_COUNTRY':
            return Object.assign({}, state, {
                isLoading: true,
                modalVisible:true,
                resetFormData:true,

            })
        case 'SUCCESS_ADD_NEW_COUNTRY':
            return Object.assign({}, state, {
                isLoading: false,
                modalVisible:false,
                resetFormData:false,

            })
        case 'FAILED_ADD_NEW_CATEGORY':
            return Object.assign({},state,{
                isLoading:false,
                modalVisible:true,
                errorMessage: action.err,
                resetFormData:false,
            })
        case 'REQUEST_UPDATE_COUNTRY':
            return Object.assign({},state,{
                isLoading:true,
                modalVisible:true,
                resetFormData:true,
            })
        case 'SUCCESSS_UPDATE_COUNTRY':
            return Object.assign({},state,{
                isLoading:false,
                modalVisible:false,
                resetFormData:false,
            })
        case 'FAILED_UPDATE_COUNTRY':
            return Object.assign({},state,{
                isLoading:false,
                modalVisible:false,
                resetFormData:false,
            })
        default:
            return state
    }
}