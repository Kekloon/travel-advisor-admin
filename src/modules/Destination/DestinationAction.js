import { firestore, storage } from '../../config/firebase'
import { uploadFile } from '../../config/helper'
import { message } from 'antd'

const checkIsDestinationNameExist = (destinationName) => {
    return new Promise((resolve, reject) => {
        let query = firestore.collection('destination').where('destinationName', '==', destinationName)
        query.get()
            .then((val) => {
                if (val.docs.length > 0) {
                    resolve(true)
                    message.error('This Destination name was exsited !! ')
                }
                else {
                    resolve(false)
                }
            }).catch((e) => reject(e))
    })
}

// ----------------------- get destination list ------------------- //

export const getDestinationList = () => (dispatch) => {
    dispatch(_requestGetDestinationList())
    try {
        firestore.collection('destination').onSnapshot(() => {
            firestore.collection('destination').get()
                .then((val) => {
                    let destinationList = []
                    val.forEach((doc) => {
                        destinationList.push(doc.data())
                    })
                    dispatch(_sucessGetDestinationList(destinationList))
                }).catch((e) => dispatch(_failGetDestinationList(e)))

        })
    } catch (e) {
        dispatch(_failGetDestinationList(e))
    }
}

    export const _requestGetDestinationList = () => ({
        type: 'REQUEST_GET_DestinationList'
    })

    export const _sucessGetDestinationList = (payload) => ({
        type: 'SUCESS_GET_DESTINATIONList',
        payload
    })

    export const _failGetDestinationList = (err) => ({
        type: 'FAILED_GET_DESTINATIONList',
        err,

    })

// ------------------------ delete destination -------------------------//

export const deleteDestination = (data) => async (dispatch) => {
    dispatch(_requestDeleteDestinationList())
    let destinationRef = firestore.collection('destination').doc(data.destinationId)
    let destinationCoverImageFile = storage.refFromURL(data.destinationCoverImage)
    if (data.destinationImage !== null) {
        await data.destinationImage.map((img, key) => {
            let destinationImageFile = storage.refFromURL(img)
            destinationImageFile.delete()
        })
        await destinationCoverImageFile.delete()
        await destinationRef.delete()
            .then(() => dispatch(_successDeleteDestination()))
            .then(() => message.success('Delete destination successfully'))
            .catch((e) => dispatch(_failDeleteDestination(e)))
    }
    else {
        await destinationCoverImageFile.delete()
        await destinationRef.delete()
            .then(() => dispatch(_successDeleteDestination()))
            .then(() => message.success('Delete destination successfully'))
            .catch((e) => dispatch(_failDeleteDestination(e)))

    }
}

export const _requestDeleteDestinationList = () => ({
    type: 'REQUEST_DELETE_DESTINATION'
})

export const _successDeleteDestination = () => ({
    type: 'SUCCESS_DELETE_DESTINATION'
})

export const _failDeleteDestination = (err) => ({
    type: 'FAILED_DELETE_DESTINATION',
    err,
})


// ---------------------------add new destination--------------------------//
export const addNewDestination = (destination) => async (dispatch) => {
    const result = await checkIsDestinationNameExist(destination.destinationName)
    if (!result) {
        dispatch(_requestAddNewDestination())
        let destinationRef = firestore.collection('destination')
        let destinationCoverImageUrl = await uploadFile(destination.destinationCoverImage)
        let destinationImageList = []
        // let country
        // let countryRef = firestore.collection('countryList').doc(destination.destinationCountry)
        // await countryRef.get().then((val) => {
        //     country = val.data().name
        // })
        if (destination.destinationImage !== null) {
            await Promise.all(destination.destinationImage.map(async (img) => {
                const destinationImageUrl = await uploadFile(img.originFileObj)
                destinationImageList.push(destinationImageUrl)
            }))
            destinationRef.add({}).then((doc) => {
                destinationRef.doc(doc.id).set({
                    destinationId: doc.id,
                    destinationName: destination.destinationName,
                    destinationCoverImage: destinationCoverImageUrl,
                    destinationImage: destinationImageList,
                    destinationDescription: destination.destinationDescription,
                    destinationDurationTime: destination.destinationDurationTime,
                    destinationSuggestTimeType: destination.destinationSuggestTimeType,
                    destinationCategory: destination.destinationCategory,
                    destinationTicketPrice: destination.destinationTicketPrice || null,
                    destinationCountry: destination.destinationCountry || null,
                    destinationCity: destination.destinationCity || null,
                    destinationPostalCode: destination.destinationPostalCode || null,
                    destinationAddress: destination.destinationAddress || null,
                    destinationRate: [],
                    destinationComment:[],
                    destinationLatitude: destination.destinationLatitude || null,
                    destinationLongitude:destination.destinationLongitude || null,
                })
            }).then(() => dispatch(_successAddNewDestination())).then(() => message.success('Add new destination successfully'))
                .catch((e) => dispatch(_failAddNewDestination(e)))
        }

        else {
            destinationRef.add({}).then((doc) => {
                destinationRef.doc(doc.id).set({
                    destinationId: doc.id,
                    destinationName: destination.destinationName,
                    destinationCoverImage: destinationCoverImageUrl,
                    destinationImage: null,
                    destinationDescription: destination.destinationDescription,
                    destinationDurationTime: destination.destinationDurationTime,
                    destinationCategory: destination.destinationCategory,
                    destinationSuggestTimeType: destination.destinationSuggestTimeType,
                    destinationTicketPrice: destination.destinationTicketPrice || null,
                    destinationCountry: destination.destinationCountry,
                    destinationCity: destination.destinationCity,
                    destinationPostalCode: destination.destinationPostalCode,
                    destinationAddress: destination.destinationAddress,
                    destinationRate: [],
                    destinationComment:[],
                    destinationLatitude: destination.destinationLatitude || null,
                    destinationLongitude:destination.destinationLongitude || null,
                })
            }).then(() => dispatch(_successAddNewDestination())).then(() => message.success('Add new destination successfully'))
                .catch((e) => dispatch(_failAddNewDestination(e)))
        }
    }
}

export const _requestAddNewDestination = () => ({
    type: 'REQUEST_ADD_NEW_DESTINATION',
})

export const _successAddNewDestination = () => ({
    type: 'SUCCESS_ADD_NEW_DESTINATION',
})

export const _failAddNewDestination = (err) => ({
    type: 'FAIL_ADD_NEW_DESTINATION',
    err
})

export const setDestinationData = (payload) => ({
    type: 'SET_DESTINATION_DATA',
    payload
})

export const upDateDestination = (destination) => async (dispatch) => {
    dispatch(_requestUpdateDestination())
    const destinationRef = firestore.collection('destination').doc(destination.destinationId)

    let destinationCoverFile = storage.refFromURL(destination.destinationCoverImage.thumbUrl)
    await Promise.all(destination.destinationImage.map(async (img) => {
        let destinationImageFile = storage.refFromURL(img.thumbnail)
        await destinationImageFile.delete()
    }))
    
    let uploadDestinationNewCoverFile = await uploadFile(destination.destinationReplaceCoverImage)
    let destinationImageList = []
    await Promise.all(destination.destinationEditImage.map(async (img) => {
        const uploadNewDestinationImageFile = await uploadFile(img.originFileObj)
        destinationImageList.push(uploadNewDestinationImageFile)
    }))
    await destinationCoverFile.delete()

    destinationRef.set({
        destinationId: destination.destinationId,
        destinationName: destination.destinationName,
        destinationCoverImage: uploadDestinationNewCoverFile,
        destinationImage: destinationImageList,
        destinationDescription: destination.destinationDescription,
        destinationDurationTime: destination.destinationDurationTime,
        destinationSuggestTimeType: destination.destinationSuggestTimeType,
        destinationCategory: destination.destinationCategory,
        destinationTicketPrice: destination.destinationTicketPrice || null,
        destinationCountry: destination.destinationCountry,
        destinationCity: destination.destinationCity || null,
        destinationPostalCode: destination.destinationPostalCode || null,
        destinationAddress: destination.destinationAddress || null,
        destinationLatitude: destination.destinationLatitude || null,
        destinationLongitude: destination.destinationLongitude || null,
        
    }, { merge: true })
        .then(() => dispatch(_successUpdateDestination()))
        .then(() => message.success('Update destination successfully'))
        .catch((e) => dispatch(_failedUpdateDestination(e)))
}

export const _requestUpdateDestination = () => ({
    type: 'REQUEST_UPDATE_DESTINATION'
})

export const _successUpdateDestination = () => ({
    type: 'SUCCESS_UPDATE_DESTINATION'
})

export const _failedUpdateDestination = (err) => ({
    type: 'FAILED_UPDATE_DESTINATION',
    err
})
