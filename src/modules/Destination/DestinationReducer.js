const initialState = {
    isLoading: false,
    errorMessage: '',
    destinationList: [],
    modalVisible: false,
    resetFormData: false,
    destinationData:{},
}

export default function (state = initialState, action) {
    switch (action.type) {
        case 'REQUEST_GET_DestinationList':
            return Object.assign({}, state, {
                isLoading: true
            })
        case 'SUCESS_GET_DESTINATIONList':
            return Object.assign({}, state, {
                isLoading: false,
                destinationList: action.payload
            })
        case 'FAILED_GET_DESTINATIONList':
            return Object.assign({}, state, {
                isLoading: false,
                errorMessage: action.err
            })
        case 'REQUEST_ADD_NEW_DESTINATION':
            return Object.assign({}, state, {
                isLoading: true,
                resetFormData: false,
            })
        case 'SUCCESS_ADD_NEW_DESTINATION':
            return Object.assign({}, state, {
                isLoading: false,
                resetFormData: true,
            })
        case 'FAIL_ADD_NEW_DESTINATION':
            return Object.assign({}, state, {
                isLoading: false,
                resetFormData: false,
                errorMessage: action.err
            })
        case 'REQUEST_DELETE_DESTINATION':
            return Object.assign({}, state, {
                isLoading: true,
            })
        case 'SUCCESS_DELETE_DESTINATION':
            return Object.assign({}, state, {
                isLoading: false,
            })
        case 'FAILED_DELETE_DESTINATION':
            return Object.assign({}, state, {
                isLoading: false,
                errorMessage: action.err
            })
        case 'SET_DESTINATION_DATA':
        return Object.assign({},state,{
            destinationData:action.payload,
            isLoading:false,
        })
        case'REQUEST_UPDATE_DESTINATION':
        return Object.assign({},state,{
            isLoading:true,
        })
        case 'SUCCESS_UPDATE_DESTINATION':
        return Object.assign({},state,{
            isLoading:false,
        })
        case 'FAILED_UPDATE_DESTINATION':
        return Object.assign({},state,{
            isLoading:false,
            errorMessage:action.err
        })
        default:
            return state
    }
}