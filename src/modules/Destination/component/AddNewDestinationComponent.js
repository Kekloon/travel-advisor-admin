import React, { Component } from 'react'
import { Form, Spin, Input, Button, Upload, Icon, Select, Col, Row } from 'antd'
import { __values } from 'tslib';
const FormItem = Form.Item
const Option = Select.Option

class AddNewDestinationComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
            countryList: [],
            fileList: [],
            destinationFileList: [],
            disable: true,
            destination: {},
            resetFormData:false,
        }
        // this.handleChange1 = this.handleChange1.bind(this)
        this.handleSelectChange = this.handleSelectChange.bind(this)
        this.setResetFromData = this.setResetFromData.bind(this)
        this.clearForm = this.clearForm.bind(this)

    }

    componentWillMount() {
        this.props.getCountryList()

    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isLoading: nextProps.isLoading,
            countryList: nextProps.countryList,
            resetFormData: nextProps.resetFormData,
        })
    }

    handleSubmit = (e) => {
        e.preventDefault()
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.setState({
                    destination: {
                        destinationName: values.DestinationName,
                        destinationCoverImage: values.DestinationCoverImage[0].originFileObj,
                        destinationImage: values.DestinationImage || null,
                        destinationDescription: values.DestinationDescription,
                        destinationDurationTime: values.DestinationDurationTime,
                        destinationSuggestTimeType:values.TimeSelector,
                        destinationCategory: values.DestinationCategory,
                        destinationTicketPrice: values.ticketPrice || null,
                        destinationCountry: values.DestinationCountry,
                        destinationCity: values.DestinationCity || null,
                        destinationPostalCode: values.DestinationPostalCode || null,
                        destinationAddress: values.DestinationAddress || null,
                        destinationLatitude: values.DestinationLatitude || null,
                        destinationLongitude: values.DestinationLongitude || null,

                    }
                }, () => this.props.addNewDestination(this.state.destination))

            }
        })
    }

    normFile = (e) => {
        if (e.fileList.length) {
            if (Array.isArray(e)) {
                return e
            }
            return e && e.fileList
        }
    }
    
    handleChange = ({ fileList }) => this.setState({ fileList })

    // handleChange1(fileList1) {
    //     this.setState({
    //         destinationFileList: fileList1,
    //     })

    // }

    handleSelectChange = (value) => {
        if (value === 'themePark') {
            this.setState({
                disable: false
            })
        }
        else {
            this.setState({
                disable: true
            })
        }
    }

    setResetFromData(bool) {
        if (bool === true) {
            this.props.form.resetFields();
        }

    }

    clearForm() {
        this.props.form.resetFields();
        this.setState({
            fileList: [],
            destinationFileList: [],
        })
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const SuggestDestinationTimeSelector = getFieldDecorator('TimeSelector', {
            initialValue: 'hours',
        })(
            <Select style={{ width: 110 }}>
                <Option value='hours'>hours</Option>
                <Option value='minutes'>minutes</Option>
                <Option value='days'>days</Option>
            </Select>
        );
        const { isLoading, countryList, fileList, destinationFileList } = this.state
        const uploadButton = (
            <div>
                <Icon type='plus' />
                <div className='ant-upload-text'>Upload</div>
            </div>
        );

        return (
            <Spin spinning={this.state.isLoading}>
                <Form onSubmit={this.handleSubmit}
                    onChange={this.setResetFromData(this.state.resetFormData)}
                >
                    <FormItem label='Destination name'>
                        {getFieldDecorator('DestinationName',
                            { rules: [{ required: true, message: 'Please input destination name' }] })
                            (
                            <Input prefix={<Icon type='aliwangwang' theme='outlined' style={{ color: 'rgba(0,0,0,.25)' }} />}
                                placeholder='Destination name' />
                            )
                        }
                    </FormItem>
                    <FormItem label='Destination cover image'>
                        {getFieldDecorator('DestinationCoverImage', {
                            valuePropName: 'fileList',
                            getValueFromEvent: this.normFile,
                            onChange: this.handleChange,
                            rules: [{ required: true }],
                        })(
                            <Upload action='' listType='picture-card'>
                                {fileList.length >= 1 ? null : uploadButton}
                            </Upload>
                        )}
                    </FormItem>
                    <FormItem label='Destination image'>
                        {getFieldDecorator('DestinationImage', {
                            valuePropName: 'fileList',
                            getValueFromEvent: this.normFile,
                            rules: [{ required: true }],
                            // onChange: this.handleChange1,
                        })(
                            <Upload listType='picture-card'>
                              <Icon type='plus' />
                            <div className='ant-upload-text'>Upload</div>
                            </Upload>
                        )}
                    </FormItem>
                    <FormItem label='Destination description'>
                        {getFieldDecorator('DestinationDescription',
                            { rules: [{ required: true, message: 'Please input destination description' }] })
                            (
                            <Input.TextArea style={{ height: 120 }}
                                placeholder='Destination description' />
                            )
                        }
                    </FormItem>
                    <FormItem label='Destination suggest duration'>
                        {getFieldDecorator('DestinationDurationTime', {
                            rules: [{ required: true, message: 'Please input suggest destination duration time' }],
                        })(
                            <Input addonAfter={SuggestDestinationTimeSelector} placeholder='Suggest time' style={{ width: '60%' }} />
                        )}
                    </FormItem>
                    <FormItem label='Destination Category' >
                        {getFieldDecorator('DestinationCategory', {
                            rules: [{ required: true, message: 'Please select destination category' }],
                            onChange: this.handleSelectChange

                        })(
                            <Select
                                placeholder='Please select a destination category'>
                                <Option value='tour'>Tour</Option>
                                <Option value='shopping'>Shopping</Option>
                                <Option value='museums'>Museums</Option>
                                <Option value='themePark'>ThemePark</Option>
                                <Option value='restaurant'>Restaurant</Option>
                            </Select>
                        )
                        }
                    </FormItem>
                    <FormItem label='Ticket price'>
                        {getFieldDecorator('ticketPrice')
                            (
                            <Input
                                placeholder={this.state.disable ? 'Only category theme park need to enter this field' : 'Please enter ticket price'}
                                disabled={this.state.disable} />
                            )}
                    </FormItem>
                    <FormItem label='Destination country' >
                        {getFieldDecorator('DestinationCountry', {
                            rules: [{ required: true, message: 'Please select destination country' }],
                        })(
                            <Select
                                placeholder='Please select a destination country'
                                optionFilterProp='title'
                            >
                                {countryList.map((val, key) => {
                                    return (
                                        <Option key={key} title={val.name} value={val.countryId}>{val.name}</Option>
                                    )
                                })}
                            </Select>
                        )}
                    </FormItem>
                    <FormItem label='Destination city'>
                        {getFieldDecorator('DestinationCity')
                            (
                            <Input
                                placeholder='Destination city' />
                            )
                        }
                    </FormItem>
                    <FormItem label='Destination postal code'>
                        {getFieldDecorator('DestinationPostalCode')
                            (
                            <Input
                                placeholder='Destination postal code' />
                            )
                        }
                    </FormItem>
                    <FormItem label='Destination address'>
                        {getFieldDecorator('DestinationAddress')
                            (
                            <Input
                                placeholder='Destination address' />
                            )
                        }
                    </FormItem>
                    <FormItem label='Destination latitude'>
                        {getFieldDecorator('DestinationLatitude',
                        { rules: [{ required: true, message: 'Please input destination latitude' }] })
                        
                            (
                            <Input
                                placeholder='Destination latitude' />
                            )
                        }
                    </FormItem>
                    <FormItem label='Destination longitude'>
                        {getFieldDecorator('DestinationLongitude',
                        { rules: [{ required: true, message: 'Please input destination longitude' }] })
                            (
                            <Input
                                placeholder='Destination longitude' />
                            )
                        }
                    </FormItem>
                    <FormItem>
                        <Row>
                            <Col span={8}>
                                <Button className='NewButton' htmlType='submit'>Add</Button>
                            </Col>
                            <Col span={8}>
                                <Button className='NewButton' onClick={() => this.clearForm()}>Clear</Button>
                            </Col>
                        </Row>
                    </FormItem>
                </Form>
            </Spin>
        )
    }

}


const WrappedAddDestinationForm = Form.create()(AddNewDestinationComponent)
export default WrappedAddDestinationForm