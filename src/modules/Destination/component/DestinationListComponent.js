import React, { Component } from 'react'
import { List, Button, Modal } from 'antd';
import { Link } from 'react-router-dom'
import AddNewDestinationContainer from '../container/AddNewDestinationContainer'
import EditDestinationContainer from '../container/EditDestinationContainer'

const confirm = Modal.confirm

class DestinationListComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
            destinationList: [],
            pageOfItems: [],
            currentPage: 0,
            pageSize: 4,
            visible: false,
            destinationData: null,
        }
        this.onChangePage = this.onChangePage.bind(this)
        this.setVisible = this.setVisible.bind(this)
        this.showModals = this.showModals.bind(this)
        this.showDeleteConfirm = this.showDeleteConfirm.bind(this)
        this.passDataToEditComponent = this.passDataToEditComponent.bind(this)
        this.handleClick=this.handleClick.bind(this)
    }

    componentDidMount() {
        this.props.getDestinationList()
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isLoading: nextProps.isLoading,
            destinationList: nextProps.destinationList
        }, () => this.onChangePage(1, this.state.pageSize))
    }

    setVisible(bool, data) {
        this.setState({
            visible: bool,
            destinationData: data,
        })

    }

    handleClick(data){
        this.props.setDestinationData(data)
    }

    passDataToEditComponent(data) {
        this.setState({
            destinationData: data
        });
    }

    onChangePage(page, pageSize) {
        let array = []
        this.state.destinationList.map((val, key) => {
            if (key < (page * pageSize)) {
                if (key >= ((page - 1) * pageSize)) {
                    array.push(val)
                }
            }
        })
        this.setState({
            pageOfItems: array,
            currentPage: page
        })
    }

    showDeleteConfirm(data) {
        confirm({
            title: 'Are you sure delete this Destination?',
            content: data.destinationName,
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk: () => {
                this.props.deleteDestination(data)
            },
        })
    }

    showModals() {
        this.setState({
            visible: true,
        });
    }


    render() {
        const { isLoading, destinationList, pageOfItems, pageSize } = this.state

        const pagination = {
            total: destinationList.length,
            showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`,
            onChange: (page, pageSize) => this.onChangePage(page, pageSize),
            pageSize: pageSize,
            defaultCurrent: 1,
            currentPage: this.state.currentPage,
        }
        return (
            <div>
                <div style={{ width: '100%', display: 'table' }}>
                    <span className='title-text'>Destination List</span>
                    <div style={{ textAlign: 'right', display: 'table-cell' }}>
                        <Modal
                            title='Add New Destination'
                            centered
                            visible={this.state.visible}
                            onCancel={() => this.setVisible(false)}
                            footer={[]}

                        >
                            <AddNewDestinationContainer setVisible={this.setVisible} />
                        </Modal>
                        <Button className='NewButton' onClick={() => this.showModals()}>Add new Destination</Button>
                    </div>
                </div>
                <List
                    className='demo-loadmore-list'
                    loading={isLoading}
                    itemLayout='horizontal'
                    dataSource={pageOfItems}
                    pagination={pagination}
                    renderItem={data => (
                        <List.Item
                            actions={[
                                <Link to='/destinationList/detail'><Button onClick={()=>this.handleClick(data)}>View detail</Button></Link>,
                                <Button onClick={() => this.showDeleteConfirm(data)}>Delete</Button>
                            ]}
                        >
                            <List.Item.Meta
                                avatar={<img src={data.destinationCoverImage} height='90px' />}
                                title={data.destinationName}
                                description={data.destinationDescription}
                                content={data.destinationCountry}
                            />
                        </List.Item>
                    )}
                />
            </div>
        )
    }
}

export default DestinationListComponent