import React, { Component } from 'react'
import { Form, Input, Button, Select, Spin, Icon, Row, Col, Upload } from 'antd'
import _ from 'lodash'
import { ContentBackspace } from 'material-ui/svg-icons';
import image from 'material-ui/svg-icons/image/image';
const Option = Select.Option
const FormItem = Form.Item
import { Link } from 'react-router-dom'


class EditDestinationComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            countryList: [],
            isLoading: false,
            fileList: [],
            destinationData: {},
            coverImage: [],
            Image: null,
            disable:false,
            destinationEditData:{}
            
        }
        this.handleSelectChange = this.handleChange.bind(this)
        this.removeCoverImage = this.removeCoverImage.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    componentWillMount() {
        this.props.getCountryList()
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            countryList: nextProps.countryList,
            isLoading: nextProps.isLoading,
        })

        if (nextProps.destinationData.destinationCoverImage && nextProps.destinationData.destinationImage) {
            const data = [];
            nextProps.destinationData.destinationImage.map((val, key) => {
                const abc = [{
                    uid: key,
                    thumbnail: val,
                    url: val,
                }]
                data.push(abc[0])
                this.setState({
                    Image: data,
                })

            })
            this.setState({
                destinationData: nextProps.destinationData,
                coverImage: [{
                    uid: '0',
                    thumbUrl: nextProps.destinationData.destinationCoverImage
                }],
            })
        }
    }

    removeCoverImage() {
        this.setState({
            coverImage: null,
        })
    }



    handleDataUpdate = (e) => {
        e.preventDefault()
        this.props.form.validateFields(async (err, values) => {
            if (!err) {
                this.setState({
                    destinationEditData:{
                        destinationId:this.state.destinationData.destinationId,
                        destinationName: values.DestinationName,
                        destinationCoverImage: values.DestinationCoverImage[0],
                        destinationImage: values.DestinationImage || null,
                        destinationDescription: values.DestinationDescription,
                        destinationDurationTime: values.DestinationDurationTime,
                        destinationSuggestTimeType:values.TimeSelector,
                        destinationCategory: values.DestinationCategory,
                        destinationTicketPrice: values.ticketPrice || null,
                        destinationCountry: values.DestinationCountry,
                        destinationCity: values.DestinationCity || null,
                        destinationPostalCode: values.DestinationPostalCode || null,
                        destinationAddress: values.DestinationAddress || null,
                        destinationEditImage:values.DestinationEditImage,
                        destinationReplaceCoverImage:values.DestinationReplaceCoverImage[0].originFileObj,
                        destinationLatitude: values.DestinationLatitude || null,
                        destinationLongitude: values.DestinationLongitude || null,

                    }
                },()=>this.props.upDateDestination(this.state.destinationEditData))
            }
        })
    }

    handleSelectChange = (value) => {
        if (value === 'themePark') {
            this.setState({
                disable: false
            })
        }
        else {
            this.setState({
                disable: true
            })
        }
    }

    normFile = (e) => {
        if (e.fileList.length) {
            if (Array.isArray(e)) {
                return e
            }
            return e && e.fileList
        }
    }

    handleChange = ({ fileList }) => this.setState({ fileList })


    render() {
        const { countryList, fileList, destinationData, Image } = this.state
        const { getFieldDecorator } = this.props.form
        // const uploadButton = (
        //     <div>
        //         <Icon type='plus' />
        //         <div className='ant-upload-text'>Upload</div>
        //     </div>
        // )
        const SuggestDestinationTimeSelector = getFieldDecorator('TimeSelector', {
            initialValue: destinationData.destinationSuggestTimeType,
        })(
            <Select style={{ width: 110 }}>
                <Option value='hours'>hours</Option>
                <Option value='minutes'>minutes</Option>
                <Option value='days'>days</Option>
            </Select>
        );

        const uploadButton = (
            <div>
                <Icon type='plus' />
                <div className='ant-upload-text'>Upload</div>
            </div>
        );
        return (
            <Form onSubmit={this.handleDataUpdate}>
                <Spin spinning={this.state.isLoading}>
                    <FormItem label='Destination name'>
                        {getFieldDecorator('DestinationName',
                            {
                                initialValue: destinationData.destinationName,
                                rules: [{ required: true, message: 'Please input destination name' }]
                            })
                            (
                            <Input prefix={<Icon type='aliwangwang' theme='outlined' style={{ color: 'rgba(0,0,0,.25)' }} />}
                                placeholder='Destination name' />
                            )
                        }
                    </FormItem>
                    <Row>
                        <Col span={4}>
                            <FormItem label='Destination cover image'>
                                {getFieldDecorator('DestinationCoverImage', {
                                    valuePropName: 'fileList',
                                    onChange: this.handleChange,
                                    initialValue: this.state.coverImage ? this.state.coverImage : [],
                                })
                                    (
                                    <Upload
                                        onRemove={false}
                                        listType='picture-card'
                                    />
                                    )}
                            </FormItem>
                        </Col>
                        <Col span={4}>
                            <FormItem label ='Destination replace cover iamge'>
                            {getFieldDecorator('DestinationReplaceCoverImage', {
                            valuePropName: 'fileList',
                            getValueFromEvent: this.normFile,
                            onChange: this.handleChange,
                            rules: [{ required: true }],
                        })(
                            <Upload action='' listType='picture-card'>
                                {fileList.length >= 1 ? null : uploadButton}
                            </Upload>
                        )}
                            </FormItem>
                        </Col>
                    </Row>
                    <FormItem label='Destination image'>
                        {getFieldDecorator('DestinationImage', {
                            valuePropName: 'fileList',
                            getValueFromEvent: this.normFile,
                            initialValue: Image !== null ? [...Image] : [],
                        })
                            (
                            <Upload
                                onRemove={false}
                                listType='picture-card'
                            />

                            )}
                    </FormItem>
                    <FormItem label='Replace destination image'>
                        {getFieldDecorator('DestinationEditImage', {
                            valuePropName: 'fileList',
                            getValueFromEvent: this.normFile,
                            rules: [{ required: true }],
                        })
                            (
                            <Upload
                                listType='picture-card'
                            >
                                {<Icon type='plus'> <div className='ant-upload-text'>Replace</div></Icon>}
                            </Upload>

                            )}
                    </FormItem>
                    <FormItem label='Destination description'>
                        {getFieldDecorator('DestinationDescription',
                            {
                                initialValue: destinationData.destinationDescription,
                                rules: [{ required: true, message: 'Please input destination description' }]
                            })
                            (
                            <Input.TextArea style={{ height: 120 }}
                                placeholder='Destination description' />
                            )
                        }
                    </FormItem>
                    <FormItem label='Destination suggest duration'>
                        {getFieldDecorator('DestinationDurationTime', {
                            initialValue: destinationData.destinationDurationTime,
                            rules: [{ required: true, message: 'Please input suggest destination duration time' }],
                        })(
                            <Input addonAfter={SuggestDestinationTimeSelector} placeholder='Suggest time' style={{ width: '60%' }} />
                        )}
                    </FormItem>
                    <FormItem label='Destination Category' >
                        {getFieldDecorator('DestinationCategory', {
                            initialValue: destinationData.destinationCategory,
                            rules: [{ required: true, message: 'Please select destination category' }],
                            // onChange: this.handleSelectChange
                        })(
                            <Select
                                placeholder='Please select a destination category'>
                                <Option value='tour'>Tour</Option>
                                <Option value='shopping'>Shopping</Option>
                                <Option value='museums'>Museums</Option>
                                <Option value='themePark'>ThemePark</Option>
                                <Option value='restaurant'>Restaurant</Option>
                            </Select>
                        )
                        }
                    </FormItem>
                    <FormItem label='Ticket price'>
                        {getFieldDecorator('ticketPrice', {
                            initialValue: destinationData.destinationTicketPrice || null,
                        })
                            (
                            <Input
                                placeholder={this.state.disable ? 'Only category theme park need to enter this field' : 'Please enter ticket price'}
                                disabled={this.state.disable} />
                            )}
                    </FormItem>
                    {console.log(destinationData.destinationCountry)}
                    <FormItem label='Destination country' >
                        {getFieldDecorator('DestinationCountry',
                            {
                                initialValue: destinationData.destinationCountry,
                                rules: [{ required: true, message: 'Please select destination country' }],
                            })(
                                <Select
                                    placeholder='Please select a destination country'
                                    optionFilterProp='title'
                                >
                                    {countryList.map((val, key) => {
                                        return (
                                            <Option key={key} title={val.name} value={val.countryId}>{val.name}</Option>
                                        )
                                    })}
                                </Select>
                            )}
                    </FormItem>
                    <FormItem label='Destination city'>
                        {getFieldDecorator('DestinationCity', {
                            initialValue: destinationData.destinationCity,
                        })
                            (
                            <Input
                                placeholder='Destination city' />
                            )
                        }
                    </FormItem>
                    <FormItem label='Destination postal code'>
                        {getFieldDecorator('DestinationPostalCode', {
                            initialValue: destinationData.destinationPostalCode,
                        })
                            (
                            <Input
                                placeholder='Destination postal code' />
                            )
                        }
                    </FormItem>
                    <FormItem label='Destination address'>
                        {getFieldDecorator('DestinationAddress', {
                            initialValue: destinationData.destinationAddress,
                        })
                            (
                            <Input
                                placeholder='Destination address' />
                            )
                        }
                    </FormItem>
                    <FormItem label='Destination latitude'>
                        {getFieldDecorator('DestinationLatitude', {
                            initialValue: destinationData.destinationLatitude,
                            rules: [{ required: true, message: 'Please input destination latitude' }]
                        })
                            (
                            <Input
                                placeholder='Destination address' />
                            )
                        }
                    </FormItem>
                    <FormItem label='Destination longitude'>
                        {getFieldDecorator('DestinationLongitude', {
                            initialValue: destinationData.destinationLongitude,
                            rules: [{ required: true, message: 'Please input destination longitude' }]
                        })
                            (
                            <Input
                                placeholder='Destination address' />
                            )
                        }
                    </FormItem>
                    <FormItem>
                        <Row>
                            <Col span={8}>
                                <Button className='NewButton' htmlType='submit'>Edit</Button>
                            </Col>
                            <Col span={8}>
                                {/* <Button className='NewButton' onClick={() => this.clearForm()}>Clear</Button> */}
                            </Col>
                        </Row>
                    </FormItem>
                </Spin>
            </Form>
        )
    }
}
const WrappedEditDestinationComponent = Form.create()(EditDestinationComponent)
export default WrappedEditDestinationComponent