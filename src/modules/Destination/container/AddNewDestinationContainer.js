import { connect } from 'react-redux'
import AddNewDestinationComponent from '../component/AddNewDestinationComponent'
import { getCountryList } from '../../CountryList/countryListAction'
import { addNewDestination } from '../DestinationAction'

const mapStateToProps = (state) => ({
    isLoading: state.destinationReducer.isLoading,
    countryList: state.countryListReducer.countryList,
    resetFormData:state.destinationReducer.resetFormData,
})

const mapDispatchToProps = (dispatch) => ({
    getCountryList: () => {
        dispatch(getCountryList())
    },
    addNewDestination: (destination) => {
        dispatch(addNewDestination(destination))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(AddNewDestinationComponent)