import { connect } from 'react-redux'
import DestinationListComponent from '../component/DestinationListComponent'
import { getDestinationList, deleteDestination,setDestinationData } from '../DestinationAction'

const mapStateToProps = (state) => ({
    isLoading: state.destinationReducer.isLoading,
    destinationList: state.destinationReducer.destinationList,
})

const mapDispatchToProps = (dispatch) => ({
    getDestinationList:()=>{
        dispatch(getDestinationList())
    },
    deleteDestination:(data)=>{
        dispatch(deleteDestination(data))
    },
    setDestinationData:(data)=>{
        dispatch(setDestinationData(data))
    },
   

})

export default connect(mapStateToProps,mapDispatchToProps)(DestinationListComponent)