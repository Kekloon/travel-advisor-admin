import { connect } from 'react-redux'
import EditDestinationComponent from '../component/EditDestinationComponent'
import { getCountryList } from '../../CountryList/countryListAction'
import { upDateDestination } from '../DestinationAction'

const mapStateToProps = (state) => ({
    isLoading: state.destinationReducer.isLoading,
    countryList: state.countryListReducer.countryList,
    destinationData: state.destinationReducer.destinationData,
})

const mapDispatchToProps = (dispatch) => ({
    getCountryList: () => {
        dispatch(getCountryList())
    },
    upDateDestination: (data) => {
        dispatch(upDateDestination(data))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(EditDestinationComponent)