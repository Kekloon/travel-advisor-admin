import { firestore } from '../../config/firebase'
import { uploadFile, deleteFile } from '../../config/helper'
import { message } from 'antd'

export const getForumList = () => (dispatch) => {
    dispatch(_requestGetForumList())
    const forumRef = firestore.collection('forum')

    try {
        forumRef.onSnapshot(async (forums) => {
            let forumList = []
            await Promise.all(forums.docs.map((doc) => {
                const forum = {
                    ...doc.data(),
                    id: doc.id
                }
                forumList.push(forum)
            }))
            dispatch(_successGetForumList(forumList))
        })
    } catch (e) {
        dispatch(_failedGetForumList(e))
    }
}

const _requestGetForumList = () => ({
    type: 'REQUEST_GET_FORUM_LIST'
})
const _successGetForumList = (payload) => ({
    type: 'SUCCESS_GET_FORUM_LIST',
    payload
})
const _failedGetForumList = (err) => ({
    type: 'FAILED_GET_FORUM_LIST',
    err
})

export const createForum = (forum) => async (dispatch) => {
    dispatch(_requestCreateForum())
    const forumRef = firestore.collection('forum')

    const imageUrl = await uploadFile(forum.file)

    forumRef.add({
        title: forum.title,
        description: forum.description,
        imageUrl: imageUrl,
        posts: [],
        isActive: true
    }).then(() => dispatch(_successCreateForum()))
        .then(() => message.success('Success create new forum!'))
        .catch((e) => dispatch(_failedCreateForum(e)))

}

const _requestCreateForum = () => ({
    type: 'REQUEST_CREATE_FORUM'
})
const _successCreateForum = () => ({
    type: 'SUCCESS_CREATE_FORUM'
})
const _failedCreateForum = () => ({
    type: 'FAILED_CREATE_FORUM'
})

export const setForumStatus = (forumId, bool) => {
    const forumRef = firestore.collection('forum').doc(forumId)

    forumRef.set({
        isActive: bool
    }, { merge: true })
        .then(() => message.success('Change forum status success!'))
        .catch((e) => message.error('Failed: ', e))

}

export const updateForum = (forum) => async (dispatch) => {
    dispatch(_requestUpdateForum())
    const forumRef = firestore.collection('forum').doc(forum.id)
    try {
        if (forum.file) {
            await deleteFile(forum.imageUrl)
            const imageUrl = await uploadFile(forum.file)
            await forumRef.set({
                title: forum.title,
                description: forum.description,
                imageUrl: imageUrl
            }, { merge: true })
        } else {
            await forumRef.set({
                title: forum.title,
                description: forum.description,
            }, { merge: true })
        }
        message.success('Update successful!')
        dispatch(_successUpdateForum())
    } catch (e) {
        message.error('Error: ', e)
        dispatch(_failedUpdateForum(e))
    }
}

const _requestUpdateForum = () => ({
    type: 'REQUEST_UPDATE_FORUM'
})
const _successUpdateForum = () => ({
    type: 'SUCCESS_UPDATE_FORUM'
})
const _failedUpdateForum = (err) => ({
    type: 'FAILED_UPDATE_FORUM',
    err
})