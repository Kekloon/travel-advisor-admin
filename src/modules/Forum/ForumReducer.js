const initialState = {
    forumList: {
        isLoading: false,
        payload: [],
        erroMessage: null,
    },
    createForum: {
        isCreating: false,
        success: false,
    },
    updateForum: {
        isUpdating: false,
        success: false,
    }
}

export default function (state = initialState, action) {
    switch (action.type) {
        case 'REQUEST_GET_FORUM_LIST':
            return Object.assign({}, state, {
                forumList: {
                    isLoading: true
                }
            })
        case 'SUCCESS_GET_FORUM_LIST':
            return Object.assign({}, state, {
                forumList: {
                    isLoading: false,
                    payload: action.payload
                }
            })
        case 'FAILED_GET_FORUM_LIST':
            return Object.assign({}, state, {
                forumList: {
                    isLoading: false,
                    erroMessage: action.err
                }
            })
        case 'REQUEST_CREATE_FORUM':
            return Object.assign({}, state, {
                createForum: {
                    isCreating: true,
                    success: false,
                }
            })
        case 'SUCCESS_CREATE_FORUM':
            return Object.assign({}, state, {
                createForum: {
                    isCreating: false,
                    success: true,
                }
            })
        case 'FAILED_CREATE_FORUM':
            return Object.assign({}, state, {
                createForum: {
                    isCreating: false,
                    success: false,
                }
            })
        case 'REQUEST_UPDATE_FORUM':
            return Object.assign({}, state, {
                updateForum: {
                    isUpdating: true,
                    success: false,
                }
            })
        case 'SUCCESS_UPDATE_FORUM':
            return Object.assign({}, state, {
                updateForum: {
                    isUpdating: false,
                    success: true,
                }
            })
        case 'FAILED_UPDATE_FORUM':
            return Object.assign({}, state, {
                updateForum: {
                    isUpdating: false,
                    success: false,
                    erroMessage: action.err
                }
            })
        default:
            return state
    }
}