import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { updateForum } from '../ForumAction'
import { Form, Input, Button, Icon, Spin, Upload } from 'antd'
const FormItem = Form.Item

class EditForumForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isUpdating: false,
            forum: null,
        }
    }


    componentWillMount(){
        this.setState({ forum: this.props.forum })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isUpdating: nextProps.isUpdating,
            forum: nextProps.forum
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                const forum = {
                    id: this.state.forum.id,
                    title: values.title,
                    description: values.description,
                    file: values.upload ? values.upload[0].originFileObj : null,
                    imageUrl: this.state.forum.imageUrl
                }
                this.props.updateForum(forum)
                this.setState({
                    isUpdating: true
                })
            }
        });
    }

    normFile = (e) => {
        // Upload event
        if (e.fileList.length <= 1) {
            if (Array.isArray(e)) {
                return e;
            }
            this.setState({
                file: e.file
            })
            return e && e.fileList
        }
    }

    render() {
        const { forum } = this.state
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 8,
                },
            },
        };
        return (
            <Spin spinning={this.state.isUpdating} >
                <Form onSubmit={this.handleSubmit}>
                    <FormItem
                        {...formItemLayout}
                        label='Title'
                    >
                        {getFieldDecorator('title', {
                            rules: [{ required: true, message: 'Please input forum title!', whitespace: true }],
                            initialValue: forum.title
                        })(
                            <Input />
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label='Description'
                    >
                        {getFieldDecorator('description', {
                            rules: [{ required: true, message: 'Please input category name!', whitespace: true }],
                            initialValue: forum.description
                        })(
                            <Input />
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label='Image'
                    >
                        {getFieldDecorator('upload', {
                            valuePropName: 'fileList',
                            getValueFromEvent: this.normFile,
                            // rules: [{ required: true }]
                        })(
                            <Upload name='logo' action='' listType='picture'>
                                <Button>
                                    <Icon type='upload' /> Click to upload
                                </Button>
                            </Upload>
                        )}
                    </FormItem>
                    <FormItem {...tailFormItemLayout}>
                        <Button type='primary' htmlType='submit' >Update</Button>
                    </FormItem>
                </Form>
            </Spin>
        )
    }
}

EditForumForm.propTypes = {
    forum: PropTypes.object,
    form: PropTypes.object,
    isUpdating: PropTypes.bool,
    updateForum: PropTypes.func
}

const WrapEditForumForm = Form.create()(EditForumForm)

const mapStatToProps = (state) => ({
    isUpdating: state.forum.updateForum.isUpdating
})
const mapDispatchToProps = (dispatch) => ({
    updateForum: (forum) => {
        dispatch(updateForum(forum))
    }
})

export default connect(mapStatToProps, mapDispatchToProps)(WrapEditForumForm)