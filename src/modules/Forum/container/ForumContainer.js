import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { Button, Modal, Table, Divider } from 'antd'
import { getForumList, setForumStatus } from '../ForumAction'
import CreateForumForm from '../component/CreateForumForm'
import EditForumForm from '../component/EditForumForm'
const { Column } = Table

class ForumContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            modalVisible: false,
            editModalVisible: false,
            forumList: [],
            toEditForum: null,
        }
    }

    componentWillMount() {
        this.props.getForumList()
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            forumList: nextProps.forumList
        })
        if (nextProps.isCreateSuccess) {
            this.setState({ modalVisible: false })
        }

        if(nextProps.isUpdateSuccess){
            this.setState({ editModalVisible: false })
        }
    }

    formModal() {
        return (
            <Modal
                title='Create new forum'
                wrapClassName='vertical-center-modal'
                visible={this.state.modalVisible}
                onCancel={() => this.setState({ modalVisible: false })}
                footer={[]}
            >
                <div>
                    <CreateForumForm />
                </div>
            </Modal>
        )
    }

    editFormModal() {
        return (
            <Modal
                title='Edit forum'
                wrapClassName='vertical-center-modal'
                visible={this.state.editModalVisible}
                onCancel={() => this.setState({ editModalVisible: false })}
                footer={[]}
            >
                <div>
                    <EditForumForm forum={this.state.toEditForum} />
                </div>
            </Modal>
        )
    }

    render() {
        const { forumList } = this.state

        return (
            <div style={{ backgroundColor: 'white' }}>
                {this.formModal()}
                {this.editFormModal()}
                <div style={{ width: '100%', display: 'table' }}>
                    <h3 style={{ display: 'table-cell', padding: '1em' }} >Forum List</h3>
                    <div style={{ textAlign: 'right', display: 'table-cell' }}>
                        <Button onClick={() => this.setState({ modalVisible: true })} style={{ marginRight: '2em' }} type='primary'>Add</Button>
                    </div>
                </div>
                <Table dataSource={forumList} style={{ margin: '1em' }} rowKey='id' >
                    <Column
                        title='Image'
                        dataIndex='imageUrl'
                        width='1%'
                        render={(data, obj) => {
                            return <img src={data} height='90px' />
                        }}
                    />
                    <Column
                        title='Title'
                        dataIndex='title'
                    />
                    <Column
                        title='Description'
                        dataIndex='description'
                    />
                    <Column
                        title='Status'
                        dataIndex='isActive'
                        render={(data) => (
                            <span>
                                {data ? 'Active' : 'Inactive'}
                            </span>
                        )}
                    />
                    <Column
                        title='Action'
                        key='action'
                        dataIndex='id'
                        render={(data, obj) => (
                            <span>
                                <Divider type='vertical' />
                                <a href='#'
                                    onClick={() => this.setState({ toEditForum: obj, editModalVisible: true })}
                                >
                                    Edit
                            </a>
                                <Divider type='vertical' />
                                <a href='#'
                                    onClick={() => obj.isActive ? this.props.setForumStatus(data, false) : this.props.setForumStatus(data, true)} >
                                    {obj.isActive ? 'Disable' : 'Enable'}
                                </a>
                                <Divider type='vertical' />
                            </span>
                        )}
                    />
                </Table>
            </div>
        )
    }
}

ForumContainer.propTypes = {
    getForumList: PropTypes.func,
    forumList: PropTypes.array,
    isCreateSuccess: PropTypes.bool,
    setForumStatus: PropTypes.func,
    isUpdateSuccess: PropTypes.bool,
}

const mapStateToProps = (state) => ({
    isLoading: state.forum.forumList.isLoading,
    forumList: state.forum.forumList.payload,
    isCreateSuccess: state.forum.createForum.success,
    isUpdateSuccess: state.forum.updateForum.success
})
const mapDispatchToProps = (dispatch) => ({
    getForumList: () => {
        dispatch(getForumList())
    },
    setForumStatus: (forumId, bool) => {
        dispatch(setForumStatus(forumId, bool))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(ForumContainer)