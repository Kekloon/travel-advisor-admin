import { firestore, storage } from '../../config/firebase'
import { uploadFile } from '../../config/helper'
import { message } from 'antd'
import HotelListComponent from './component/HotelListComponent';

const checkIsHotelNameExist = (hotelName) => {
    return new Promise((resolve, reject) => {
        let query = firestore.collection('hotel').where('hotelName', '==', hotelName)
        query.get()
            .then((val) => {
                if (val.docs.length > 0) {
                    resolve(true)
                    message.error('This hotel name was exsited !!')
                }
                else {
                    resolve(false)
                }
            }).catch((e) => reject(e))
    })
}

export const getHotelList = () => (dispatch) => {
    dispatch(_requestGetHotelList())
    try {
        firestore.collection('hotel').onSnapshot(() => {
            firestore.collection('hotel').get()
                .then((val) => {
                    let hotelList = []
                    val.forEach((doc) => {
                        hotelList.push(doc.data())
                    })
                    dispatch(_sucessGetHotelList(hotelList))
                }).catch((e) => dispatch(_failGetHotelList(e)))

        })
    } catch (e) {
        dispatch(_failGetHotelList(e))
    }
}

export const _requestGetHotelList = () => ({
    type: 'REQUEST_GET_HOTEL_LIST'
})

export const _sucessGetHotelList = (payload) => ({
    type: 'SUCCESS_GET_HOTELLIST',
    payload
})

export const _failGetHotelList = (err) => ({
    type: 'FAILED_GET_HOTELLIST',
    err,
})

export const deleteHotel = (data) => async (dispatch) => {
    dispatch(_requestDeleteHotel())
    let hotelRef = firestore.collection('hotel').doc(data.hotelId)
    let hotelCoverImage = storage.refFromURL(data.hotelCoverImage)
    if (data.hotelImage !== null) {
        await data.hotelImage.map((img, key) => {
            let hotelImageFile = storage.refFromURL(img)
            hotelImageFile.delete()
        })
        await hotelCoverImage.delete()
        await hotelRef.delete()
            .then(() => dispatch(_successDeleteHotel()))
            .then(() => message.success('Delete hotel successfully'))
            .catch((e) => dispatch(_failDeleteHotel(e)))
    }
}

export const _requestDeleteHotel = () => ({
    type: 'REQUEST_DELETE_HOTEL'
})

export const _successDeleteHotel = () => ({
    type: 'SUCCESS_DELETE_HOTEL'
})

export const _failDeleteHotel = (err) => ({
    type: 'FAILED_DELETE_HOTEL',
    err,
})


export const addNewHotel = (hotel) => async (dispatch) => {
    const result = await checkIsHotelNameExist(hotel.hotelName)
    if (!result) {
        console.log(hotel.hotelCoverImage)
        dispatch(_requestAddNewHotel())
        let hotelRef = firestore.collection('hotel')
        let hotelCoverImageUrl = await uploadFile(hotel.hotelCoverImage)
        let hotelImageList = []

        await Promise.all(hotel.hotelImage.map(async (img) => {
            const hotelImageUrl = await uploadFile(img.originFileObj)
            hotelImageList.push(hotelImageUrl)
        }))

        hotelRef.add({}).then((doc) => {
            hotelRef.doc(doc.id).set({
                hotelId: doc.id,
                hotelName: hotel.hotelName,
                hotelCoverImage: hotelCoverImageUrl,
                hotelImage: hotelImageList,
                hotelDescription: hotel.hotelDescription,
                hotelAmenities: hotel.hotelAmenities,
                hotelPackage: hotel.hotelPackage,
                hotelCountry: hotel.hotelCountry,
                hotelCity: hotel.hotelCity || null,
                hotelPostalCode: hotel.hotelPostalCode || null,
                hotelAddress: hotel.hotelAddress || null,
                hotelRate:[],
                hotelComment:[],
                hotelLatitude:hotel.hotelLatitude,
                hotelLongitude:hotel.hotelLongitude,
            })
        }).then(() => dispatch(_successAddHotel())).then(() => message.success('Add new hotel successfully.'))
            .catch((e) => dispatch(_failedAddHotel()))
    }

}

export const _requestAddNewHotel = () => ({
    type: 'REQUEST_ADD_NEW_HOTEL'
})

export const _successAddHotel = () => ({
    type: 'SUCCESS_ADD_HOTEL'
})

export const _failedAddHotel = (err) => ({
    type: 'FAILED_ADD_HOTEL',
    err
})

export const upDateHotel = (hotel) => async (dispatch) => {
    dispatch(_requestUpdateHotel())
    const hotelRef = firestore.collection('hotel').doc(hotel.hotelId)

    let hotelCoverFile = storage.refFromURL(hotel.hotelCoverImage[0].thumbUrl)
    await Promise.all(hotel.hotelImage.map(async (img) => {
        let hotelImageFile = storage.refFromURL(img.thumbnail)
        await hotelImageFile.delete()
    }))
    await hotelCoverFile.delete()

    let uploadHotelNewCoverFile = await uploadFile(hotel.hotelReplaceCoverImage)
    let hotelImageList = []
    await Promise.all(hotel.hotelEditImage.map(async (img) => {
        const uploadNewHotelImageFile = await uploadFile(img.originFileObj)
        hotelImageList.push(uploadNewHotelImageFile)
    }))

    hotelRef.set({
        hotelId: hotel.hotelId,
        hotelName: hotel.hotelName,
        hotelCoverImage: uploadHotelNewCoverFile,
        hotelImage: hotelImageList,
        hotelDescription: hotel.hotelDescription,
        hotelAmenities: hotel.hotelAmenities,
        hotelPackage: hotel.hotelPackage,
        hotelCountry: hotel.hotelCountry,
        hotelCity: hotel.hotelCity || null,
        hotelPostalCode: hotel.hotelPostalCode || null,
        hotelAddress: hotel.hotelAddress || null,
        hotelLatitude:hotel.hotelLatitude,
        hotelLongitude:hotel.hotelLongitude,
        
    }, { merge: true })
        .then(() => dispatch(_successUpdateHotel()))
        .then(() => message.success('Update hotel successfully'))
        .catch((e) => dispatch(_failedUpdateHotel(e)))
}

export const _requestUpdateHotel = () => ({
    type: 'REQUEST_UPDATE_HOTEL'
})

export const _successUpdateHotel = () => ({
    type: 'SUCCESS_UPDATE_HOTEL'
})

export const _failedUpdateHotel = (err) => ({
    type: 'FAILED_UPDATE_HOTEL',
    err
})