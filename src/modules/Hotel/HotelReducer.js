const initialState = {
    isLoading: false,
    errMessage: '',
    hotelList: [],
    modalVisible: false,
}

export default function (state = initialState, action) {
    switch (action.type) {
        case 'REQUEST_GET_HOTEL_LIST':
            return Object.assign({}, state, {
                isLoading: true
            })
        case 'SUCCESS_GET_HOTELLIST':
            return Object.assign({}, state, {
                isLoading: false,
                hotelList: action.payload
            })
        case 'FAILED_GET_HOTELLIST':
            return Object.assign({}, state, {
                isLoading: false,
                errMessage: action.err
            })
        case 'REQUEST_DELETE_HOTEL':
            return Object.assign({}, state, {
                isLoading: true
            })
        case 'SUCCESS_DELETE_HOTEL':
            return Object.assign({}, state, {
                isLoading: false
            })
        case 'FAILED_DELETE_HOTEL':
            return Object.assign({}, state, {
                isLoading: false,
                errMessage: action.err
            })
        case 'REQUEST_ADD_NEW_HOTEL':
            return Object.assign({}, state, {
                isLoading: true
            })
        case 'SUCCESS_ADD_HOTEL':
            return Object.assign({}, state, {
                isLoading: false,
            })
        case 'FAILED_ADD_HOTEL':
            return Object.assign({}, state, {
                isLoading: false,
                errMessage: action.err
            })
        case 'REQUEST_UPDATE_HOTEL':
            return Object.assign({},state,{
                isLoading:true
            })
        case 'SUCCESS_UPDATE_HOTEL':
            return Object.assign({},state,{
                isLoading:false,
            })
        case 'FAILED_UPDATE_HOTEL':
            return Object.assign({},state,{
                isLoading:false,
                errMessage:action.err
            })
        default:
            return state
    }
} 