import React, { Component } from 'react'
import { Form, Input, Button, Select, Spin, Icon, Row, Col, Upload } from 'antd'

const Option = Select.Option
const FormItem = Form.Item

class EditHotelComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            countryList: [],
            isLoading: false,
            fileList: [],
            coverImage: [],
            Image: [],
            initialKey: [],
            hotelData:{},
        }
        this.handleChange = this.handleChange.bind(this)
    }

    componentWillMount() {
        this.props.getCountryList()

        this.setState({
            coverImage: [{
                uid: '0',
                thumbUrl: this.props.data.hotelCoverImage,
            }]
        })

        const temp = [];
        this.props.data.hotelImage.map((val, key) => {
            const abc = [{
                uid: key,
                thumbnail: val,
                url: val,
            }]
            temp.push(abc[0])
            this.setState({
                Image: temp,
            })
        })

        const key = []
        this.props.data.hotelPackage.map((val, keys) => {
            key.push(keys)
            this.setState({
                initialKey: key,

            })
        })



    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            countryList: nextProps.countryList,
            isLoading: nextProps.isLoading
        })
    }


    handleDataUpdate = (e) => {
        e.preventDefault()
        let data=[]
        this.props.form.validateFields(async (err,values)=>{
            if(!err){
                values.hotelPackageName.map((dataName, key) => {
                    const temp = {
                        hotelPackageName: dataName,
                        
                    }
                    data.push(temp)
                })
                values.hotelBedType.map((dataBedType, key) => {
                    data[key].hotelBedType=dataBedType
                })
                values.hotelPrice.map((dataPrice, key) => {
                    data[key].hotelPrice=dataPrice
                    
                })
                values.MaximumGuest.map((dataGuest, key) => {
                    data[key].MaximumGuest=dataGuest
                })
                this.setState({
                    hotelData:{
                            hotelId:this.props.data.hotelId,
                            hotelName:values.HotelName,
                            hotelCoverImage:values.HotelCoverImage,
                            hotelReplaceCoverImage:values.HotelReplaceCoverImage[0].originFileObj,
                            hotelImage:values.HotelImage,
                            hotelEditImage:values.HotelEditImage,
                            hotelDescription:values.HotelDescription,
                            hotelAmenities:values.HotelAmenities,
                            hotelPackage:data,
                            hotelCity:values.HotelCity,
                            hotelPostalCode:values.HotelPostalCode,
                            hotelAddress:values.HotelAddress,
                            hotelCountry:values.HotelCountry,
                            hotelLatitude:values.HotelLatitude,
                            hotelLongitude:values.HotelLongitude,

                    }
                },()=>this.props.upDateHotel(this.state.hotelData))

            }
        })
    }

    normFile = (e) => {
        if (e.fileList.length) {
            if (Array.isArray(e)) {
                return e
            }
            return e && e.fileList
        }
    }

    remove = k => {
        const { form } = this.props;
        const keys = form.getFieldValue("keys");
        if (keys.length !== k + 1) {
            return;
        }

        form.setFieldsValue({
            keys: keys.filter(key => key !== k)
        });
    };

    add = () => {
        const { form } = this.props;
        const keys = form.getFieldValue("keys");
        const nextKeys = keys.concat(keys.length);
        form.setFieldsValue({
            keys: nextKeys
        });
    };

    handleChange = ({ fileList }) => this.setState({ fileList })

    render() {
        const { data } = this.props
        const { countryList, fileList } = this.state
        const { getFieldDecorator, getFieldValue } = this.props.form;
        const UploadButton = (
            <div>
                <Icon type='plus' />
                <div className='ant-upload-text'>Upload</div>
            </div>
        );
        getFieldDecorator('keys', { initialValue: [...this.state.initialKey] });
        const keys = getFieldValue('keys');
        const hotelPackage = keys.map((k, index) => {
            return (
                <div key={k} style={{ backgroundColor: '#fafafa' }}>
                    <FormItem label={`Hotel package name ${k + 1}`} required={false}>
                        {getFieldDecorator(`hotelPackageName[${k}]`, {
                            rules: [
                                {
                                    required: true,
                                    whitespace: true,
                                    message: "Please input hotel package detail or delete this field",
                                }
                            ],
                            initialValue: data.hotelPackage[k] ? data.hotelPackage[k].hotelPackageName : [],
                        })(
                            <Input
                                placeholder='Hotel package name'
                                style={{ width: "80%", marginRight: 8 }}
                            />
                        )}
                        {keys.length > 0 ?
                            (
                                <Icon
                                    className='dynamic-delete-button'
                                    type='minus-circle-o'
                                    disabled={keys.length === 1}
                                    onClick={() => this.remove(k)}
                                />
                            ) : null}
                    </FormItem>
                    <FormItem label={'Hotel price'} required={false}>
                        {getFieldDecorator(`hotelPrice[${k}]`, {
                            rules: [
                                {
                                    required: true,
                                    whitespace: true,
                                    message: "Please input hotel price or delete this field"
                                }
                            ],
                            initialValue: data.hotelPackage[k] ? data.hotelPackage[k].hotelPrice : []

                        })(
                            <Input
                                placeholder='Hotel price'
                                style={{ width: "80%", marginRight: 8 }}
                            />
                        )}
                    </FormItem>
                    <FormItem label='Hotel bed type' required={false}>
                        {getFieldDecorator(`hotelBedType[${k}]`, {
                            validateTrigger: ["onChange", "onBlur"],
                            rules: [
                                {
                                    required: true,
                                    whitespace: true,
                                    message: "Please input hotel bed type or delete this field",
                                    type: 'array'
                                }
                            ],
                            initialValue: data.hotelPackage[k] ? data.hotelPackage[k].hotelBedType : []
                        })(
                            <Select style={{ width: "80%", marginRight: 8 }} mode='multiple' placeholder='Please select hotel bed type' optionFilterProp='title'>
                                <Option value='1 King bed'>1 King bed</Option>
                                <Option value='2 King bed'>2 King bed</Option>
                                <Option value='1 Queen bed'>1 Queen bed </Option>
                                <Option value='2 Queen bed'>2 Queen bed</Option>
                                <Option value='1 Single bed'>1 Single bed</Option>
                                <Option value='2 Single bed'>2 Single bed</Option>
                                <Option value='3 Single bed'>3 Single bed</Option>
                            </Select>
                        )}
                    </FormItem>
                    <FormItem label='Maximum guest' required={false}>
                        {getFieldDecorator(`MaximumGuest[${k}]`, {
                            validateTrigger: ["onChange", "onBlur"],
                            rules: [
                                {
                                    required: true,
                                    whitespace: true,
                                    message: "Please input Maximum guest or delete this field"
                                }
                            ],
                            initialValue: data.hotelPackage[k] ? data.hotelPackage[k].MaximumGuest:[]
                        })(
                            <Input
                                placeholder='Maximum guest'
                                style={{ width: "80%", marginRight: 8 }}
                            />
                        )}
                    </FormItem>

                </div>
            );
        });

        return (
            <Spin spinning={this.state.isLoading}>
                <Form onSubmit={this.handleDataUpdate} style={{ width: 400 }}>
                    <FormItem label='Hotel name'>
                        {getFieldDecorator('HotelName',
                            {
                                initialValue: data.hotelName,
                                rules: [{ required: true, message: 'Please input hotel name' }]
                            })
                            (
                            <Input prefix={<Icon type='aliwangwang' theme='outlined' style={{ color: 'rgba(0,0,0,.25)' }} />}
                                placeholder='Hotel name' />
                            )
                        }
                    </FormItem>
                    <Row>
                        <Col span={8}>
                            <FormItem label='Hotel cover image'>
                                {getFieldDecorator('HotelCoverImage', {
                                    valuePropName: 'fileList',
                                    onChange: this.handleChange,
                                    initialValue: this.state.coverImage ? this.state.coverImage : []
                                })
                                    (
                                    <Upload
                                        onRemove={false}
                                        listType='picture-card'
                                    />
                                    )}
                            </FormItem>
                        </Col>
                        <Col span={8}>
                            <FormItem label='Hotel replace cover iamge'>
                                {getFieldDecorator('HotelReplaceCoverImage', {
                                    valuePropName: 'fileList',
                                    getValueFromEvent: this.normFile,
                                    onChange: this.handleChange,
                                    rules: [{ required: true }],
                                })(
                                    <Upload action='' listType='picture-card'>
                                        {fileList.length >= 1 ? null : UploadButton}
                                    </Upload>
                                )}
                            </FormItem>
                        </Col>
                    </Row>
                    <FormItem label='Hotel image'>
                        {getFieldDecorator('HotelImage', {
                            valuePropName: 'fileList',
                            getValueFromEvent: this.normFile,
                            initialValue: this.state.Image ? [...this.state.Image] : [],
                        })
                            (
                            <Upload
                                onRemove={false}
                                listType='picture-card'
                            />

                            )}
                    </FormItem>
                    <FormItem label='Replace hotel image'>
                        {getFieldDecorator('HotelEditImage', {
                            valuePropName: 'fileList',
                            getValueFromEvent: this.normFile,
                            rules: [{ required: true }],
                        })
                            (
                            <Upload
                                listType='picture-card'
                            >
                                {<Icon type='plus'> <div className='ant-upload-text'>Replace</div></Icon>}
                            </Upload>

                            )}
                    </FormItem>
                    <FormItem label='Hotel description'>
                        {getFieldDecorator('HotelDescription',
                            {
                                rules: [{ required: true, message: 'Please input hotel description' }],
                                initialValue: data.hotelDescription,
                            })
                            (
                            <Input.TextArea style={{ height: 128 }}
                                placeholder='Hotel description' />
                            )
                        }
                    </FormItem>
                    <FormItem label='Hotel amenities'>
                        {getFieldDecorator('HotelAmenities',
                            {
                                rules: [{ required: true, message: 'Please input hotel amenities' }],
                                initialValue: data.hotelAmenities
                            })
                            (
                            <Select mode='multiple' placeholder='Please select hotel amenities' optionFilterProp='title'>
                                <Option value='Wifi'>Wifi</Option>
                                <Option value='Breakfast'>Breakfast</Option>
                                <Option value='Free parking'>Free parking</Option>
                                <Option value='Kitchen'>Kitchen</Option>
                            </Select>
                            )
                        }
                    </FormItem>
                    <FormItem>
                        <Button type='dashed' onClick={this.add} style={{ width: "60%" }}>
                            <Icon type='plus' /> Add hotel package
                        </Button>
                        {hotelPackage}
                    </FormItem>
                    <FormItem label='Hotel country'>
                        {getFieldDecorator('HotelCountry', {

                            rules: [{ required: true, message: 'Please select hotel country' }],
                            initialValue: data.hotelCountry,
                        })(
                            <Select
                                placeholder='Please select a hotel country'
                                optionFilterProp='title'
                            >
                                {countryList.map((val, key) => {
                                    return (
                                        <Option key={key} title={val.name} value={val.countryId}>{val.name}</Option>
                                    )
                                })}
                            </Select>
                        )}
                    </FormItem>
                    <FormItem label='Hotel city'>
                        {getFieldDecorator('HotelCity', {
                            initialValue: data.hotelCity
                        })
                            (
                            <Input
                                placeholder='Hotel city' />
                            )
                        }
                    </FormItem>
                    <FormItem label='Hotel postal code'>
                        {getFieldDecorator('HotelPostalCode', {
                            initialValue: data.hotelPostalCode
                        })
                            (
                            <Input
                                placeholder='Hotel postal code' />
                            )
                        }
                    </FormItem>
                    <FormItem label='Hotel address'>
                        {getFieldDecorator('HotelAddress', {
                            initialValue: data.hotelAddress
                        })
                            (
                            <Input
                                placeholder='Hotel address' />
                            )
                        }
                    </FormItem>
                    <FormItem label='Hotel latitude'>
                        {getFieldDecorator('HotelLatitude', {
                            initialValue: data.hotelLatitude,
                            rules: [{ required: true, message: 'Please input hotel latitude' }]
                        })
                            (
                            <Input
                                placeholder='Hotel latitude' />
                            )
                        }
                    </FormItem>
                    <FormItem label='Hotel longitude'>
                        {getFieldDecorator('HotelLongitude', {
                            initialValue: data.hotelLongitude,
                            rules: [{ required: true, message: 'Please input hotel longitude' }]
                        })
                            (
                            <Input
                                placeholder='Hotel longitude' />
                            )
                        }
                    </FormItem>
                    <FormItem>
                        <Button className='NewButton' htmlType='submit'>Edit</Button>
                    </FormItem>
                </Form>
            </Spin>
        )
    }

}

const WrappedEidtHotelComponent = Form.create()(EditHotelComponent)
export default WrappedEidtHotelComponent