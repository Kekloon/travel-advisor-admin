import React, { Component } from 'react'
import { List, Button, Modal } from 'antd'
import AddHotelContainer from '../container/AddHotelContainer'
import EditHotelContainer from '../container/EditHotelContainer'

const confirm = Modal.confirm

class HotelListComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
            hotelList: [],
            pageOfItems: [],
            currentPage: 0,
            pageSize: 50,
            modalVisible: false,
            modalType: 'AddNewHotel',
            hotelData: {},
        }
        this.setModalVisible = this.setModalVisible.bind(this)
        this.showDeleteConfirm = this.showDeleteConfirm.bind(this)
        this.onChangePage = this.onChangePage.bind(this)

    }

    componentWillMount() {
        this.props.getHotelList()
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isLoading: nextProps.isLoading,
            hotelList: nextProps.hotelList
        }, () => this.onChangePage(1, this.state.pageSize))
    }

    setModalVisible(bool, type, data) {
        this.setState({
            modalVisible: bool,
            modalType: type,
            hotelData: data,
        })
    }

    onChangePage(page, pageSize) {
        let array = []
        this.state.hotelList.map((val, key) => {
            if (key < (page * pageSize)) {
                if (key >= ((page - 1) * pageSize)) {
                    array.push(val)
                }
            }
        })
        this.setState({
            pageOfItems: array,
            currentPage: page
        })
    }

    showModal() {
        const { modalType, hotelData } = this.state
        if (modalType === 'AddNewHotel') {
            return (
                <Modal
                    title='New Hotel'
                    centered
                    visible={this.state.modalVisible}
                    onCancel={() => this.setModalVisible(false)}
                    footer={[]}
                >
                    <AddHotelContainer setModalVisible={this.setModalVisible} />
                </Modal>
            )
        }
        else if (modalType === 'Edit') {
            return (
                <Modal
                    title='Edit hotel'
                    centered
                    visible={this.state.modalVisible}
                    onCancel={() => this.setModalVisible(false)}
                    footer={[]}
                >
                    <EditHotelContainer data={hotelData} />
                </Modal>
            )
        }
    }

    showDeleteConfirm(data) {
        confirm({
            title: 'Are you sure delete this hotel?',
            content: data.hotelName,
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk: () => {
                this.props.deleteHotel(data)
            },
            onCancel() { },
        })
    }

    render() {
        const { isLoading, hotelList, pageOfItems, pageSize } = this.state
        const pagination = {
            total: hotelList.length,
            showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`,
            onChange: (page, pageSize) => this.onChangePage(page, pageSize),
            pageSize: pageSize,
            defaultCurrent: 1,
            currentPage: this.state.currentPage,
        }
        const loadMore = this.showModal()
        return (
            <div>
                <div style={{ width: '100%', display: 'table' }}>
                    <span className='title-text'>Hotel List</span>
                    <div style={{ textAlign: 'right', display: 'table-cell' }}>
                        <Button className='NewButton' onClick={() => this.setModalVisible(true, 'AddNewHotel')}>Add new hotel</Button>
                    </div>
                </div>
                <List
                    className='demo-loadmore-list'
                    loading={isLoading}
                    itemLayout='horizontal'
                    dataSource={pageOfItems}
                    pagination={pagination}
                    loadMore={loadMore}
                    renderItem={data => (
                        <List.Item
                            actions={[
                                <Button onClick={() => this.setModalVisible(true, 'Edit', data)}>Edit</Button>,
                                <Button onClick={() => this.showDeleteConfirm(data)}>Delete</Button>
                            ]}
                        >
                            <List.Item.Meta
                                avatar={<img src={data.hotelCoverImage} height='90px' />}
                                title={data.hotelName}
                                description={data.hotelDescription}
                            />
                        </List.Item>
                    )}
                />
            </div>

        )
    }

}

export default HotelListComponent