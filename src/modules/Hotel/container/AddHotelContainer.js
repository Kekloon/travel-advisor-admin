import { connect } from 'react-redux'
import AddHotelComponent from '../component/AddHotelComponent'
import { getCountryList } from '../../CountryList/countryListAction'
import { addNewHotel } from '../HotelAction'

const mapStateToProps =(state)=>({
    isLoading:state.hotelReducer.isLoading,
    countryList:state.countryListReducer.countryList
})

const mapDispatchToProps =(dispatch)=>({
    getCountryList:()=>{
        dispatch(getCountryList())
    },
    addNewHotel:(hotel)=>{
        dispatch(addNewHotel(hotel))
    }

})

export default connect(mapStateToProps,mapDispatchToProps)(AddHotelComponent)