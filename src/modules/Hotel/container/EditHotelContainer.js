import { connect } from 'react-redux'
import EditHotelComponent from '../component/EditHotelComponents'
import { getCountryList } from '../../CountryList/countryListAction'
import { upDateHotel } from '../HotelAction'

const mapStateToProps =(state) => ({
    isLoading:state.hotelReducer.isLoading,
    countryList:state.countryListReducer.countryList,
})

const mapDispatchToProps =( dispatch )=>({
    getCountryList:()=>{
        dispatch(getCountryList())
    },
    upDateHotel:(data)=>{
        dispatch(upDateHotel(data))
    }
    
})

export default connect(mapStateToProps, mapDispatchToProps)(EditHotelComponent)