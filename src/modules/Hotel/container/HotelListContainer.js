import { connect } from 'react-redux'
import HotelListComponent from '../component/HotelListComponent'
import { getHotelList, deleteHotel } from '../HotelAction'

const mapStateToProps = (state)=>({
    isLoading: state.hotelReducer.isLoading,
    hotelList:state.hotelReducer.hotelList,
})

const mapDispatchToProps = (dispatch)=>({
    getHotelList:()=>{
        dispatch(getHotelList())
    },
    deleteHotel:(data)=>{
        dispatch(deleteHotel(data))
    },
 
})

export default connect(mapStateToProps, mapDispatchToProps)(HotelListComponent)