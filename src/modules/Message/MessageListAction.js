import { firestore, storage } from '../../config/firebase'

export const getMessageList = () => async (dispatch) => {
    dispatch(_requestGetMessageList())
    try {

        const privateMessageRef = firestore.collection('privateMessage')
        await privateMessageRef.onSnapshot(async () => {
            let pendingMessageList = []
            let messageList = []
            let list = []
            const privateMessageRefs = firestore.collection('privateMessage').where('subOwner', '==', 'Admin')
            privateMessageRefs.get().then(async (data) => {
                await data.forEach(async (doc) => {
                    pendingMessageList.push(doc.data())
                })
                messageList = []
                await Promise.all(pendingMessageList.map(async (val) => {
                    await firestore.collection('user').doc(val.Owner).get().then(async (userData) => {
                        list = {
                            ...val,
                            userData: userData.data()
                        }
                        messageList.push(list)
                    })
                }))
                dispatch(_successGetMessageList(messageList))
            })
        })
    }
    catch (e) {
        dispatch(_failedGetMessageList(e))
    }
}

const _requestGetMessageList = () => ({
    type: 'REQUEST_GET_MESSAGE_LIST'
})

const _successGetMessageList = (payload) => ({
    type: 'SUCCESS_GET_MESSAGE_LIST',
    payload
})

const _failedGetMessageList = (err) => ({
    type: 'FAILED_GET_MESSAGE_LIST',
    err
})

export const getRealTimePrivateMessage = (chatRoomId) => async (dispatch) => {
    dispatch(_requestGetRealTimePrivateMessage())
    const privateMessageRef = firestore.collection('privateMessage').doc(chatRoomId)
    await privateMessageRef.onSnapshot((doc) => {
        dispatch(_sucessGetRealTimePrivateMessage(doc.data()))
    })
}

const _requestGetRealTimePrivateMessage = () => ({
    type: 'REQUEST_GET_REAL_TIME_PRIVATE_MESSAGE'
})

const _sucessGetRealTimePrivateMessage = (payload) => ({
    type: 'SUCCESS_GET_REAL_TIME_PRIVATE_MESSAGE',
    payload
})

export const sendPrivateMessage = (commentMessage, chatRoomId, userData) => async (dispatch) => {
    let privateMessageList = []

    const privateMessageRef = firestore.collection('privateMessage').doc(chatRoomId)
    await privateMessageRef.get().then((val)=>{
        privateMessageList=val.data().Message
    })

    const newMessage = {
        MessageDate: new Date(),
        Word: commentMessage,
        Sender: 'Admin',
        Receiver: userData.userId
    }

    privateMessageList.push(newMessage)
    privateMessageRef.set({
        Message: privateMessageList,
        LastEdit: new Date()
    }, { merge: true })

}