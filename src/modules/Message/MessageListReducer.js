const initialState ={
    getMessageList:{
        messageList:null,
        isLoading:false,
        errorMessage:null,
    },
    getRealTimePrivateMessage:{
        messageList:null,
        isLoading:false,
    },
}

export default function (state = initialState,action){
    switch(action.type){
        case 'REQUEST_GET_MESSAGE_LIST':
        return Object.assign({},state,{
            getMessageList:{
                isLoading:true,
            }        
        })
        case 'SUCCESS_GET_MESSAGE_LIST':
        return Object.assign({},state,{
            getMessageList:{
                messageList:action.payload,
                isLoading:false,
            }
        })
        case 'FAILED_GET_MESSAGE_LIST':
        return Object.assign({},state,{
            getMessageList:{
                isLoading:false,
                errorMessage:action.payload,
            }
        })
        case 'REQUEST_GET_REAL_TIME_PRIVATE_MESSAGE':
        return Object.assign({},state,{
            getRealTimePrivateMessage:{
                isLoading:true,
            },
        })
        case 'SUCCESS_GET_REAL_TIME_PRIVATE_MESSAGE':
        return Object.assign({},state,{
            getRealTimePrivateMessage:{
                messageList:action.payload,
                isLoading:false,
            },
        })
        default: 
        return state
    }

}