import React, { Component } from 'react'
import { List, Button, Modal } from 'antd';
import { Link } from 'react-router-dom'
import _ from 'lodash'
import PrivateMessageList from '../container/PrivateMessageListContainer'
import moment from 'moment'

class MessageListComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
            messageList: [],
            userData: {},
            pendingMessageList: [],
            privateMessageData: {},
            isModalVisible: false,

        }
        this.setModalBoxVisible = this.setModalBoxVisible.bind(this)

    }

    componentDidMount() {
        this.props.getMessageList()
    }

    componentWillReceiveProps(nextProps) {
        if (!_.isEmpty(nextProps.messageList)) {
            nextProps.messageList.map((val) => {
                const latestMessageIndex = val.Message.length - 1
                const latestMessage = val.Message[latestMessageIndex]
                let list = {
                    ...val,
                    latestMessage: latestMessage
                }
                this.state.pendingMessageList.push(list)
            })

            this.setState({
                messageList: _.orderBy(this.state.pendingMessageList, ['LastEdit'], ['desc']),
                isLoading: nextProps.isLoading,
                pendingMessageList: [],
            })

        }

    }

    setModalBoxVisible(data) {
        this.setState({
            isModalVisible: true,
            privateMessageData: data,
        })
    }

    render() {

        const defaultProfileImage = require('../../../assets/user_avatar.png')
        return (
            <div>
                <Modal
                width={1000}
                title='Private Message'
                centered
                visible={this.state.isModalVisible}
                onCancel={() => this.setState({ isModalVisible: false })}
                footer={[]}
            >
                <PrivateMessageList  privateMessageData={this.state.privateMessageData} />
            </Modal>
                <div style={{ width: '100%', display: 'table' }}>
                    <span className='title-text'>
                        Message List
            </span>
                </div>
                <List
                    className='demo-loadmore-list'
                    loading={this.state.isLoading}
                    itemLayout='horizontal'
                    dataSource={this.state.messageList}
                    renderItem={data => (
                        <List.Item
                            actions={[
                                <Button onClick={() => this.setModalBoxVisible(data)}>Open Message</Button>
                            ]}
                        >
                            <List.Item.Meta
                                avatar={!_.isEmpty(data.userData.userProfilePicture) ? <img src={data.userData.userProfilePicture} height='90px' /> : <img src={defaultProfileImage} height='90px' />}
                                title={data.userData.userName}
                                description={data.latestMessage.Sender === 'Admin' ? 'You : ' + data.latestMessage.Word : data.userData.userName + ' : ' + data.latestMessage.Word}
                                content={moment(data.LastEdit).format('DD/MM/YYYY')}
                            />
                            <div style={{ position:"absolute", left:0,marginLeft:85,marginTop:10 }}>{moment(data.LastEdit).format('DD/MM/YYYY')}</div>
                        </List.Item>
                    )}
                />
            </div>
        )
    }

}

export default MessageListComponent 