import React, { Component } from 'react'
import { Form, Input, Button, Upload, Icon, Spin, Card, Avatar, Row, Col } from 'antd';
import _ from 'lodash'
import moment from 'moment'
const Search = Input.Search;


const { Meta } = Card;

class PrivateMessageListComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            privateMessage: {},
            isLoading: true,
            messageList: {},
            commentMessage:'',
        }

        this.handleBtnSend=this.handleBtnSend.bind(this)
        this.handleChange=this.handleChange.bind(this)
    }

    componentWillMount() {
        this.setState({
            privateMessage: this.props.privateMessageData,

        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            messageList: nextProps.messageList,
            isLoading: nextProps.isLoading,
        })
    }
    componentDidMount() {
        this.props.getRealTimePrivateMessage(this.props.privateMessageData.ChatRoomId)
        this.scrollToBottom()
    }

    scrollToBottom() {
        this.el.scrollIntoView({ behavior: 'smooth' });
    }

    handleChange(e){
        this.setState({
            commentMessage:e.target.value
        })
    }

    handleBtnSend(){
        if(!_.isEmpty(this.state.commentMessage)){
            this.props.sendPrivateMessage(this.state.commentMessage,this.props.privateMessageData.ChatRoomId,this.props.privateMessageData.userData)
                this.setState({
                    commentMessage:''
                })
        }
        this.scrollToBottom();

    }

    renderPrivateMessage(val, key) {
        // console.log(this.state.privateMessage)
        // console.log(val)
        const defaultProfileImage = require('../../../assets/user_avatar.png')
        return (
            <div key={key}>
                {
                    val.Sender !== 'Admin' ?
                        <Row type="flex" justify="start" align="top">
                            <Card style={{ width: 500, marginTop: 10, backgroundColor: '#EEEEEE',marginBottom:10  }}>
                                <Meta
                                    avatar={<Avatar src={!_.isEmpty(this.state.privateMessage.userData.userProfilePicture) ? this.state.privateMessage.userData.userProfilePicture : defaultProfileImage} />}
                                    title={this.state.privateMessage.userData.userName}
                                />
                                <p>{moment(val.MessageDate, 'day').fromNow()}</p>
                                <p style={{ color: 'black' }}>{val.Word}</p>
                            </Card>
                        </Row> :
                        <Row type="flex" justify="end" align="top">
                            <Card style={{ width: 500, marginTop: 10, backgroundColor: '#EEEEEE',marginBottom:10 }}>
                                <Meta
                                    avatar={<Avatar src={defaultProfileImage} />}
                                    title='Advisor'
                                />
                                <p>{moment(val.MessageDate, 'day').fromNow()}</p>
                                <p style={{ color: 'black' }}>{val.Word}</p>
                            </Card>
                        </Row>

                }
            </div>


        )
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    render() {
        return (
            <div>
                {
                    !_.isEmpty(this.state.messageList) ?
                        this.state.messageList.Message.map((val, key) => {
                            return this.renderPrivateMessage(val, key)
                        })
                        : null
                }
                <div  ref={el => { this.el = el; }}>
                    <Search style={{ marginTop:10,marginBottom:-10 }}
                    onSearch={value => this.handleBtnSend(value)}
                    size='large'
                    enterButton='Send'
                    placeholder='Write your message here...'
                    value={this.state.commentMessage}
                    onChange={this.handleChange}
                    />
                </div>
            </div>


        )
    }

}

export default PrivateMessageListComponent