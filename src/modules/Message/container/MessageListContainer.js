import { connect } from 'react-redux'
import MessageListComponent from '../component/MessageListComponent'
import { getMessageList } from '../MessageListAction'

const mapStateToProps = (state) => ({
    isLoading: state.messageReducer.getMessageList.isLoading,
    messageList: state.messageReducer.getMessageList.messageList,
})

const mapDispatchToProps = (dispatch) => ({
    getMessageList:()=>{
        dispatch(getMessageList())
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(MessageListComponent)