import { connect } from 'react-redux'
import PrivateMessageListComponent from '../component/PrivateMessageListComponent'
import { getRealTimePrivateMessage,sendPrivateMessage } from '../MessageListAction'

const mapStateToProps = (state) => ({
    isLoading: state.messageReducer.getRealTimePrivateMessage.isLoading,
    messageList: state.messageReducer.getRealTimePrivateMessage.messageList,
})

const mapDispatchToProps = (dispatch) => ({
    getRealTimePrivateMessage:(chatRoomId)=>{
        dispatch(getRealTimePrivateMessage(chatRoomId))
    },
    sendPrivateMessage:(commentMessage,chatRoomId,userData)=>{
        dispatch(sendPrivateMessage(commentMessage,chatRoomId,userData))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(PrivateMessageListComponent)