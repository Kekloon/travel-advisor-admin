const initialState = {
    getAllSlideshow: {
        isLoading: false,
        payload: [],
        errroMessage: null
    },
    createSlide: {
        isCreating: false,
        success: false,
        errroMessage: null
    }
}

export default function(state = initialState, action) {
    switch (action.type) {
        case 'REQUEST_GET_ALL_SLIDESHOW':
            return Object.assign({}, state, {
                getAllSlideshow: {
                    isLoading: true
                }
            })
        case 'SUCCESS_GET_ALL_SLIDESHOW':
            return Object.assign({}, state, {
                getAllSlideshow: {
                    isLoading: false,
                    payload: action.payload
                }
            })
        case 'FAILED_GET_ALL_SLIDESHOW':
            return Object.assign({}, state, {
                getAllSlideshow: {
                    isLoading: false,
                    errroMessage: action.err
                }
            })
        case 'REQUEST_CREATE_SLIESHOW':
            return Object.assign({}, state, {
                createSlide: {
                    isCreating: true,
                    success: false
                }
            })
        case 'SUCCESS_CREATE_SLIESHOW':
            return Object.assign({}, state, {
                createSlide: {
                    isCreating: false,
                    success: true
                }
            })
        case 'FAILED_CREATE_SLIESHOW':
            return Object.assign({}, state, {
                createSlide: {
                    isCreating: false,
                    success: false,
                    errroMessage: action.err
                }
            })
        default:
            return state
    }
}