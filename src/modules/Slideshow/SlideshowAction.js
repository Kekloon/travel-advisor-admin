import { firestore } from '../../config/firebase'
import { uploadFile } from '../../config/helper'
import { message } from 'antd'

export const getAllSlideshow = () => (dispatch) => {
    dispatch(_requestGetAllSlideshow())
    try {
        const slideshowRef = firestore.collection('slideshow')
        slideshowRef.onSnapshot(async (ref) => {
            const slideshowList = []
            await Promise.all(ref.docs.map((doc) => {
                slideshowList.push(doc.data())
            }))
            dispatch(_successGetAllSlideshow(slideshowList))
        })

    } catch (e) {
        dispatch(_failedGetAllSlideshow(e))
    }
}
const _requestGetAllSlideshow = () => ({
    type: 'REQUEST_GET_ALL_SLIDESHOW'
})
const _successGetAllSlideshow = (payload) => ({
    type: 'SUCCESS_GET_ALL_SLIDESHOW',
    payload
})
const _failedGetAllSlideshow = (err) => ({
    type: 'FAILED_GET_ALL_SLIDESHOW',
    err
})


export const createSlideshow = (slide) => async (dispatch) => {
    dispatch(_requestCreateSlideshow())
    const slideshowRef = firestore.collection('slideshow')
    const imgUrl = await uploadFile(slide.imgFile)
    try {
        slideshowRef.add({}).then((doc) => {
            slideshowRef.doc(doc.id).set({
                id: doc.id,
                title: slide.title,
                imageUrl: imgUrl
            }).then(() => dispatch(_successCreateSlideshow()))
                .then(() => message.success('Add slide successfully.'))
        })
    } catch (e) {
        dispatch(_failedCreateSlideshow(e))
    }
}

const _requestCreateSlideshow = () => ({
    type: 'REQUEST_CREATE_SLIESHOW'
})
const _successCreateSlideshow = () => ({
    type: 'SUCCESS_CREATE_SLIESHOW'
})
const _failedCreateSlideshow = (err) => ({
    type: 'FAILED_CREATE_SLIESHOW',
    err
})

export const deleteSlide = (slide) => {
    console.log('Delete slide: ', slide)
    // firestore.collection('slideshow').doc(slideId).delete()
    //     .then(() => message.success('Delete slide successfully.'))
    //     .catch((e) => message.error('Delete error! : ', e))
}