import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { createSlideshow } from '../SlideshowAction'
import { Form, Spin, Input, Upload, Button, Icon } from 'antd'
const FormItem = Form.Item
class AddSlideshowForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isCreating: false,
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isCreating: nextProps.isCreating
        })
    }


    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                if (values.upload) {
                    const slide = {
                        title: values.title,
                        imgFile: values.upload[0].originFileObj,
                    }
                    this.props.createSlideshow(slide)
                    this.setState({
                        isCreating: true
                    })
                }
            }
        });
    }

    normFile = (e) => {
        if (e.fileList.length <= 1) {
            if (Array.isArray(e)) {
                return e;
            }
            this.setState({
                file: e.file
            })
            return e && e.fileList
        }
    }


    render() {
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        }
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 8,
                },
            },
        }
        return (
            <Spin spinning={this.state.isCreating} >
                <Form onSubmit={this.handleSubmit}>
                    <FormItem
                        {...formItemLayout}
                        label='Title'
                    >
                        {getFieldDecorator('title', {
                            rules: [{ required: true, message: 'Please input category name!', whitespace: true }],
                        })(
                            <Input />
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label='Image'
                    >
                        {getFieldDecorator('upload', {
                            valuePropName: 'fileList',
                            getValueFromEvent: this.normFile,
                            rules: [{ required: true }]
                        })(
                            <Upload name='logo' action='' listType='picture'>
                                <Button>
                                    <Icon type='upload' /> Click to upload
                                </Button>
                            </Upload>
                        )}
                    </FormItem>
                    <FormItem {...tailFormItemLayout}>
                        <Button type='primary' htmlType='submit' >Create</Button>
                    </FormItem>
                </Form>
            </Spin>
        )
    }
}

AddSlideshowForm.propTypes = {
    form: PropTypes.object,
    isCreating: PropTypes.bool,
    createSlideshow: PropTypes.func,
}

const WrapAddSlideshowForm = Form.create()(AddSlideshowForm)

const mapStateToProps = (state) => ({
    isCreating: state.slideshow.createSlide.isCreating,
})
const mapDispatchToProps = (dispatch) => ({
    createSlideshow: (slide) => {
        dispatch(createSlideshow(slide))
    }
})
export default connect(mapStateToProps, mapDispatchToProps)(WrapAddSlideshowForm)
