import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { getAllSlideshow, deleteSlide } from '../SlideshowAction'
import AddSlideshowForm from '../component/AddSlideshowForm'
import { Table, Divider, Modal, Button, Popconfirm } from 'antd'
const { Column } = Table;

class SlideshowContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            slideshow: [],
            isLoading: false,
            modalVisible: false,
            editModalVisible: false,
        }
    }

    componentWillMount() {
        this.props.getAllSlideshow()
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            slideshow: nextProps.slideshows
        })
        if (nextProps.success) {
            this.setState({
                modalVisible: false
            })
        }
    }
    editModal() {
        return (
            <Modal
                title='Edit'
                wrapClassName='vertical-center-modal'
                visible={this.state.editModalVisible}
                onCancel={() => this.setState({ editModalVisible: false })}
                footer={[]}
            >
                <div>
                    Edit modal
                </div>
            </Modal>
        )
    }

    render() {
        const { slideshow } = this.state
        return (
            <div style={{ backgroundColor: 'white' }}>
                {this.editModal()}
                <div style={{ width: '100%', display: 'table' }}>
                    <h3 style={{ display: 'table-cell', padding: '1em' }} >Slideshow management</h3>
                    <div style={{ textAlign: 'right', display: 'table-cell' }}>
                        <Button onClick={() => this.setState({ modalVisible: true })} style={{ marginRight: '2em' }} type='primary'>Add</Button>
                    </div>
                </div>

                <Table dataSource={slideshow} style={{ margin: '1em' }} rowKey='id' >
                    <Column
                        title='Image'
                        dataIndex='imageUrl'
                        width='1%'
                        render={(data, obj) => {
                            return <img src={data} height='90px' />
                        }}
                    />
                    <Column
                        title='Title'
                        dataIndex='title'
                    />
                    <Column
                        title='Action'
                        dataIndex='id'
                        render={(data, obj) => (
                            <span>
                                {/* <Divider type='vertical' /> */}
                                {/* <a href='#' onClick={() => this.setState({ editModalVisible: true })} >Edit</a> */}
                                <Divider type='vertical' />
                                <Popconfirm title='Are you sure delete this slide?' onConfirm={() => this.props.deleteSlide(obj)} okText='Yes' cancelText='No'>
                                    <a href='#' >Delete</a>
                                </Popconfirm>
                                <Divider type='vertical' />
                            </span>
                        )}
                    />
                </Table>
            </div>
        )
    }
}

SlideshowContainer.propTypes = {
    getAllSlideshow: PropTypes.func,
    slideshows: PropTypes.array,
    deleteSlide: PropTypes.func,
    success: PropTypes.bool,
}

const mapStateToProps = (state) => ({
    slideshows: state.slideshow.getAllSlideshow.payload,
    success: state.slideshow.createSlide.success,
})
const mapDispatchToProps = (dispatch) => ({
    getAllSlideshow: () => {
        dispatch(getAllSlideshow())
    },
    deleteSlide: (id) => {
        dispatch(deleteSlide(id))
    },
})

export default connect(mapStateToProps, mapDispatchToProps)(SlideshowContainer)