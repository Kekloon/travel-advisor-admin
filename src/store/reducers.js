import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { reducer } from 'react-redux-sweetalert'
import { reducer as burgerMenu } from 'redux-burger-menu'

import app from '../modules/App/AppReducer'
import comic from '../modules/Comic/ComicReducer'
import category from '../modules/Category/CategoryReducer'
import author from '../modules/Author/AuthorReducer'
import user from '../modules/User/UserReducer'
import forum from '../modules/Forum/ForumReducer'
import slideshow from '../modules/Slideshow/SlideShowReducer'

// import gallery from '../modules/Gallery/GalleryReducer'
import countryListReducer from '../modules/CountryList/countryListReducer'
import destinationReducer from '../modules/Destination/DestinationReducer'
import hotelReducer from '../modules/Hotel/HotelReducer'
import messageReducer from '../modules/Message/MessageListReducer'

export const makeRootReducer = (asyncReducers) => {
  return combineReducers({
    app,
    countryListReducer,
    destinationReducer,
    hotelReducer,
    comic,
    category,
    author,
    user,
    forum,
    slideshow,
    messageReducer,
    sweetalert: reducer,
    form: formReducer,
    burgerMenu
  })
}

export const injectReducer = (store, { key, reducer }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return

  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
